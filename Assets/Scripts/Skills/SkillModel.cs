using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillModel{
  public string skillType; //attack,heal,buff,debuff
  public int? abilityPoints;
  public string targetAbility; //for buffing or debuffing armour/health

  public SkillModel(){
      skillType = null;
      abilityPoints = null;
      targetAbility = null;
  }

  public SkillModel(string type, int? strength, string target){
    skillType = type;
    abilityPoints = strength;
    targetAbility = target;
  }

  public static SkillModel GenerateSkill()
  {

		string[] skillTarget = {"armourPoints", "hitPoints", "accuracy"};
	  string[] skillTypes = {"attack", "heal", "buff", "debuff"};
		string chosenSkillTarget = null;

		int chooseSkillType = Random.Range(0,4);
	  int chooseSkillTarget = Random.Range(0,2);
		string chosenSkillType = skillTypes[chooseSkillType];

    //choose target for buff debuff
		if(chosenSkillType == "buff" || chosenSkillType=="debuff"){
	      chosenSkillTarget = skillTarget[chooseSkillTarget];
	    } else chosenSkillTarget = null;

		return new SkillModel(chosenSkillType,0,chosenSkillTarget); //change from 0 to change the strength of the skill
  }

  public static SkillModel createSkill(string typeOfSkill, int? strengthOfSkill, string targetAbility){
    SkillModel newSkill = new SkillModel(typeOfSkill,strengthOfSkill,targetAbility);
    return newSkill;
  }


}
