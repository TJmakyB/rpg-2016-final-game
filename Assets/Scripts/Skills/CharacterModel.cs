using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterModel{
  //public Inventory characterInv;
  private string _name;
  private int? _mobility;
  private int? _accuracy;
  private int? _hitPoints;
  private int? _damage;
  private int? _level;
  private int? _currentXP;
  private List<SkillModel> skills;

  public CharacterModel()
  {
	_name = "Default";
    _mobility = 10;
    _accuracy = 50;
    _hitPoints = 10;
    _damage = 10;
    _level = 1;
    _currentXP = 50;
    if(skills == null)
    {
		skills = new List<SkillModel>();
    }
  }

  public CharacterModel(string theName, int? myMobility, int? myAccuracy,int? myHealth, int? myDamage, int? myLevel, int? myCurXp)
  {
      _name=(theName);
	  _mobility=(myMobility);
	  _accuracy=(myAccuracy);
	  _hitPoints= (myHealth);
	  _damage=(myDamage);
	  _level =(myLevel);
	  _currentXP =(myCurXp);
  }

  public void setName(string CharName){
  _name = CharName;
  }
  public void addMobility(int? n){
    _mobility += n;
  }
  public void addAccuracy(int? n){
    _accuracy += n;
  }
  public void addHP(int? n){
    _hitPoints += n;
  }
  public void addDamage(int? n){
    _damage += n;
  }
  public void LevelUp(){
    _level += 1;
  }
  public void addXP(int? n){
    _currentXP += n;
        if (_currentXP >= 51)
            LevelUp();
        else
            Debug.Log(_currentXP);
  }

     public int? GetHP()
  {
      int? HP = _hitPoints;
      return HP;
  }
  public int? GetDamage()
  {
    int? damage = _damage;
    return damage;
  }
  public int? GetMobility()
  {
    int? mobility = _mobility;
    return mobility;
  }
  public int? GetAccuracy()
  {
    int? accuracy = _accuracy;
    return accuracy;
  }
  public int? GetLevel()
  {
    int? level = _level;
    return level;
  }
  public int? GetCurrXP()
  {
  	int? currentXP = _currentXP;
  	return currentXP;
  }
  public string myName()
  {
    string n = _name;
    return n;
  }

	public void SetHP(int? hp)
	{
		_hitPoints = hp;
	}
	public void SetDamage(int? myDamage)
	{
		_damage = myDamage;
	}
	public void SetMobility(int? myMobility)
	{
		_mobility = myMobility;
	}
	public void SetAccuracy(int? myAccuracy)
	{
		_accuracy = myAccuracy;
	}
	public void SetLevel(int? myLevel)
	{
		_level = myLevel;
	}
	public void setCurrentXP(int? myCurrXP)
	{
		_currentXP = myCurrXP;
	}



  public void AddSkill(SkillModel skillChosen)
  {
    //This is the function used to add a skill to a character
  }

  public static void displayChar(CharacterModel theChar){
    Debug.Log("My Name is: " + theChar.myName());
    Debug.Log("My Accuracy is: " +theChar.GetAccuracy());
	Debug.Log("My Mobility is: " +theChar.GetMobility());
	Debug.Log("My Level is: " +theChar.GetLevel());
	Debug.Log("I Deal: " +theChar.GetDamage() + " Damage");
	Debug.Log("I have: " + theChar.GetHP());
	Debug.Log("My Current XP is: " + theChar.GetCurrXP());
  }
}
