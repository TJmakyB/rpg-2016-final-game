﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AlertMessage : MonoBehaviour
{
    public static AlertMessage instance;
    public GameObject AlertPannel;
    public Text MessageDescription;
    private bool state = false;

    void Awake()
    {
        GameObject[] thisObj = GameObject.FindGameObjectsWithTag("PopUpController");
        if(thisObj.Length > 1)
        {
            if (thisObj[1] != null)
                Destroy(thisObj[1]);
        }
        instance = gameObject.GetComponent<AlertMessage>();
        DontDestroyOnLoad(gameObject);
    }
    public void ActivateMessage(string message)
    {
        TogglePannel();
        MessageDescription.text = message;

    }
    public void TogglePannel()
    {
        state = !state;
        MessageDescription.text = null;
        AlertPannel.SetActive(state);
    }
}