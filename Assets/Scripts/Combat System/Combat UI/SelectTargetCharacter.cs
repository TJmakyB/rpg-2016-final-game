﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectTargetCharacter : MonoBehaviour, IPointerDownHandler
{
    public Participants targetCharacter;
    public CombatManager CM;

    public void OnPointerDown(PointerEventData data)
    {
        if (CM != null)
        {
            if (CM.playersTurn && !CM.fightOver && targetCharacter >= Participants.enemy1)
            {
                CM.PlayerAttack(targetCharacter);
            }
        }
    }

}