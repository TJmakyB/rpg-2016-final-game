﻿/*
script description: State Machine used to manage quests and narratives
*/
using System.Collections.Generic;
using System;
using UnityEngine;

public enum FuncType {Enter, Update, Exit}

public class StateMachine<State>
{
    private class StateFunctions
    {
        public List<Action> OnEnter;
        public List<Action> OnUpdate;
        public List<Action> OnExit;

        public StateFunctions()
        {
            OnEnter = new List<Action>();
            OnUpdate = new List<Action>();
            OnExit = new List<Action>();
        }
    }

    private State curState;
    private State prevState;
    private Dictionary<State, StateFunctions> stateDiction;

    public StateMachine()
    {
        stateDiction = new Dictionary<State, StateFunctions>(20);
    }
    
    public void Initialise (State initialState)
    {
        if (!stateDiction.ContainsKey(initialState))
        {
            stateDiction.Add(initialState, new StateFunctions());
        }
        curState = initialState;
        OnEnterAction(stateDiction[initialState]);
    } 

    void OnEnterAction(StateFunctions SCB)
    {
         if(SCB.OnEnter.Count > 0)
         {
             for(int x = 0; x < SCB.OnEnter.Count; x++)
             {
                if (SCB.OnEnter[x] == null)
                    return;
                SCB.OnEnter[x]();
             }
         }
    }
    void OnUpdateAction(StateFunctions SCB)
    {
        if(SCB.OnUpdate.Count > 0)
        {
            for(int x = 0; x < SCB.OnUpdate.Count; x++)
            {
                SCB.OnUpdate[x]();
            }
        }
    }
    void OnExitAction(StateFunctions SCB)
    {
        if (SCB.OnExit.Count > 0)
        {
            for(int x = 0; x < SCB.OnExit.Count; x++)
            {
                SCB.OnExit[x]();
            }
 
        }
    }

    public void Update()
    {
        OnUpdateAction(stateDiction[curState]);
    }
    public void TransitionToState(State newState)
    {

        if(stateDiction.ContainsKey(curState))
        {
            OnExitAction(stateDiction[curState]);
        }

        if(!stateDiction.ContainsKey(newState))
        {
            stateDiction.Add(newState, new StateFunctions());
        }

        OnEnterAction(stateDiction[newState]);
        prevState = curState;
        curState = newState;
    }
    public void AddState(State state, FuncType FT, Action func)
    {
        if (!stateDiction.ContainsKey(state))
        {
            stateDiction.Add(state, new StateFunctions());
        }
        switch (FT)
        {
            case FuncType.Enter:
                AddFunc(stateDiction[state].OnEnter.Contains(func) ,func,ref stateDiction[state].OnEnter);
                return;
            case FuncType.Update:
                AddFunc(stateDiction[state].OnUpdate.Contains(func), func, ref stateDiction[state].OnUpdate);
                return;
            case FuncType.Exit:
                AddFunc(stateDiction[state].OnExit.Contains(func), func, ref stateDiction[state].OnExit);
                return;
        }

    }

    private void AddFunc(bool isIn, Action func, ref List<Action> actList )
    {
        if(!isIn)
        {
            actList.Add(func);
        }
    }
    private void RemoveState(State state)
    {
        if (stateDiction.ContainsKey(state))
        {
            stateDiction.Remove(state);
        }
    }

}