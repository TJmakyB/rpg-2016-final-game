﻿/*
script description: Contains the progression of narrative
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


[System.Serializable]
public class Conditions
{
    public WorldLocation inWorldLoc = WorldLocation.None;
    public LocalArea inLocalLoc = LocalArea.None;
    public bool ExtraConditions = false;
    public bool NeedsExtraConditions = false;
}

public class NarativeSystem : MonoBehaviour
{

    public GameController GameCon;

    public NarativeSequence[] DifferentDialogues;
    public int narativePosition = 0;
    public GameObject NarativeOverlay;
    public Narative_UI_Elements leftPanel;
    public Narative_UI_Elements rightPanel;
    public CharacterPanel[] NarativeCharacters;
    public Narative_UI_Elements blockPanel;
    public bool isTut;
    private List<NarativeDelegates> AfterNarative = new List<NarativeDelegates>();

    private List<DialogueElement> currDE = null;
    private int curPosInDialogue;
    public delegate void NarativeDelegates();
    public NarativeDelegates EndEvent;
    private string currLeft;
    private string currRight;
    private CharacterPanel currChar;

    [System.Serializable]
    public class Narative_UI_Elements
    {
        public GameObject panelObject;
        public Text NameText;
        public Image CharacterImage;
        public Text DialogueText;
    }

    [System.Serializable]
    public class CharacterPanel
    {
        public string Name;
        public Sprite characterIcon;
    }

    private void Start()
    {
        //narativePosition = 0;
        if (isTut)
        {
            NarativeEventOccurs();
        }
        InitialiseNarativeFunction();
    }
    private void InitialiseNarativeFunction()
    {
        for (int x = 0; x < DifferentDialogues.Length; x++)
        {

            DifferentDialogues[x].EventConditions.ExtraConditions = false;
            AfterNarative.Add(null);
        }
        AfterNarative[0] = GameCon.StartOfTutorial;
        AfterNarative[2] = GameCon.TavernTutorial;
        AfterNarative[4] = GameCon.EquipArouraFight;
        AfterNarative[5] = GameCon.MapTutorial;
        AfterNarative[8] = GameCon.ArenaBossBattle;
        AfterNarative[9] = GameCon.RevealNextLocation;
        AfterNarative[10] = GameCon.BeforeGraeae;
        AfterNarative[11] = GameCon.AfterGreaeFight;
        AfterNarative[13] = GameCon.RevealNextLocation;
        AfterNarative[14] = GameCon.BeforeNeameanLion;
        AfterNarative[17] = GameCon.AfterNemeanLion;
        AfterNarative[18] = GameCon.RevealNextLocation;
        AfterNarative[19] = GameCon.RevealNextLocation;
        AfterNarative[20] = GameCon.AfterDragon;
        AfterNarative[21] = GameCon.ArenaBossBattle;
        AfterNarative[22] = GameCon.MobFight;
        AfterNarative[23] = GameCon.AfterMobFight;
        AfterNarative[25] = GameCon.AfterTavernArgument;
        AfterNarative[26] = GameCon.EnterOracleTempleFade;
        AfterNarative[27] = GameCon.TheBigThreeAppear;
        AfterNarative[28] = GameCon.StartBossBattle;
        AfterNarative[30] = GameCon.StartBossBattle;
        AfterNarative[31] = GameCon.FightAthenaFinal;
        AfterNarative[32] = GameCon.HealEffect;
        AfterNarative[33] = GameCon.EndofNarrative;
    }
    public void CheckForNarative()
    {
        if (narativePosition < DifferentDialogues.Length)
        {
            if (DifferentDialogues[narativePosition].ConditionsMet())
            {
                NarativeEventOccurs();
            }
        }
    }
    public void NarativeEventOccurs()
    {
        NarativeOverlay.SetActive(true);
        currDE = DifferentDialogues[narativePosition].dialogueEvent;
        ActivateNarativePannel();
    }

    void ActivateNarativePannel()
    {
        DialogueElement currentElement = currDE[curPosInDialogue];
        if (currentElement.isBlockText)
        {
            leftPanel.panelObject.SetActive(false);
            rightPanel.panelObject.SetActive(false);
            blockPanel.panelObject.SetActive(true);
            blockPanel.DialogueText.text = currentElement.Dialogue;
            blockPanel.NameText.text = currentElement.Name;
        }
        else
        {
            blockPanel.panelObject.SetActive(false);
            string currentName = currentElement.Name;
            CharacterPanel currChar = returnCharacterPanel(currentName);
            SetUpCurrentPanel(currChar, currentElement.Dialogue);
        }
    }
    CharacterPanel returnCharacterPanel(string CharacterName)
    {
        for (int x = 0; x < NarativeCharacters.Length; x++)
        {
            if (CharacterName == NarativeCharacters[x].Name)
            {
                return NarativeCharacters[x];
            }
        }
        return null;
    }
    public void NextNarrativeElement()
    {
        curPosInDialogue++;
        if (curPosInDialogue < DifferentDialogues[narativePosition].dialogueEvent.Count)
        {
            ActivateNarativePannel();
        }
        else
            EndDialogue();
    }
    public void EndDialogue()
    {
        leftPanel.panelObject.SetActive(false);
        rightPanel.panelObject.SetActive(false);
        NarativeOverlay.SetActive(false);
        if (AfterNarative[narativePosition] != null)
            AfterNarative[narativePosition]();
        narativePosition++;
        curPosInDialogue = 0;
        CheckForNarative();
    }
    void SetUpCurrentPanel(CharacterPanel CP, string dialogue)
    {
        Narative_UI_Elements activePanel = SelectPanel(CP);
        activePanel.panelObject.SetActive(true);
        activePanel.NameText.text = CP.Name;
        activePanel.DialogueText.text = dialogue;
        activePanel.CharacterImage.sprite = CP.characterIcon;
    }
    Narative_UI_Elements SelectPanel(CharacterPanel CP)
    {
        if (currLeft == null)
        {
            currLeft = CP.Name;
            currChar = CP;
        }
        if (currChar != CP)
        {
            currRight = CP.Name;
            currChar = CP;
        }
        if (currChar != CP || CP.Name == currLeft)
        {
            currChar = CP;
            return leftPanel;
        }
        else if (currChar != CP && CP.Name == currRight)
        {
            currChar = CP;
            return rightPanel;
        }
        return leftPanel;
        //if (curPosInDialogue % 2 == 0)
        //{
        //    return leftPanel;
        //}
        //else
        //    return rightPanel;
    }
}