﻿/*
script description:
*/
using UnityEngine;
using System.Collections.Generic;
public enum Characters { Deneva, Aurora, Delvon, RippeDasphuk, Harpie, Bandit, Warrior}
public enum Attribute { Health, Damage, Level , Mobility, Acuracy}
[System.Serializable]
public abstract class CharacterBase
{
    public string Name;
    public bool isDead = false;
    public Characters thisCharacter;
    public bool isBoss = false;
    public WeaponAndArmourStats equipmentStats = new WeaponAndArmourStats();
    [SerializeField] protected CharacterAttributes StartingValues = new CharacterAttributes();
    [SerializeField] protected CharacterAttributes currentValues = new CharacterAttributes();
    [SerializeField] protected CharacterAttributes maxValues = new CharacterAttributes();
    [SerializeField] protected CharacterAttributes incrementsPerLevel = new CharacterAttributes();

    public List<BaseSkill> CurrentSkills = new List<BaseSkill>();
    public float retCurrentHealth()
    {
        return currentValues.health;
    }
    public float retMaxHealth()
    {
        return maxValues.health;
    }
    public CharacterAttributes returnCurrentValues() { return currentValues; }
    public void AdjustTargetAttribute(Attribute attri, float amount)
    {
        switch (attri)
        {
            case Attribute.Health:
                currentValues.health = AdjustAttribute(currentValues.health, maxValues.health, amount);
                return;
            case Attribute.Damage:
                currentValues.damage = (int)AdjustAttribute(currentValues.damage, maxValues.damage, amount);
                return;
            case Attribute.Level:
                currentValues.level =  (int)amount;
                return;
            default:
                return;
        }

    }
    private float AdjustAttribute(float curAtri,float maxAtri, float amount)
    {
        curAtri += amount;
        if(curAtri >= maxAtri)
        {
            curAtri = maxAtri;
        }
        if (curAtri <= 0)
            curAtri = 0;
        return curAtri;
    }
}


[System.Serializable]
public class CharacterAttributes
{
    public float health;
    public float damage;
    public int level;

    public static CharacterAttributes operator +(CharacterAttributes CA1, CharacterAttributes CA2)
    {
        CharacterAttributes temp = new CharacterAttributes();
        temp.damage = CA1.damage + CA2.damage;
        temp.health = CA1.health + CA2.health;
        temp.level = CA1.level + CA2.level;
        return temp;
    }
    public static CharacterAttributes operator *(CharacterAttributes CA1, int scalar)
    {
        CharacterAttributes CAtemp = new CharacterAttributes();
        CAtemp.damage = CA1.damage * scalar;
        CAtemp.health = CA1.health * scalar;
        CAtemp.level = CA1.level * scalar;
        return CAtemp;
    }
}
[System.Serializable]
public class WeaponAndArmourStats
{
    public int totalDamage;
    public int totalArmour;
    public int totalMobility;
    public int totalAcuracy;

    public WeaponAndArmourStats()
    {
        totalDamage = 0;
        totalAcuracy = 0;
        totalArmour = 0;
        totalMobility = 60;
    }
    public static WeaponAndArmourStats operator *(WeaponAndArmourStats stats1, int scalar)
    {
        WeaponAndArmourStats temp = new WeaponAndArmourStats();
        temp.totalDamage = stats1.totalDamage * scalar;
        temp.totalArmour = stats1.totalArmour * scalar;
        temp.totalAcuracy = stats1.totalAcuracy;
        temp.totalMobility = stats1.totalMobility;
        return temp;
    }
}
