﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BattleTypes { travel, arena, boss}
public class CharacterManager : MonoBehaviour
{
    public PlayerCharacter[] PlayableCharacters = new PlayerCharacter[4];
    public EnemyCharacter[] TravelEnemyTypes;
    public EnemyCharacter[] ArenaEnemyTypes;
    private CombatManager combMan;
    private ArenaManager arenaMan;
    private WorldAreaManager worldAreaMan;
    private LocalAreaManager localMan;
    public List<PlayerCharacter> combatMembers = new List<PlayerCharacter>();

    void Awake()
    {
        for(int x = 0; x < 1; x++ )
        {
            combatMembers.Add(PlayableCharacters[x]);
        }
        localMan = GetComponent<LocalAreaManager>();
        worldAreaMan = GetComponent<WorldAreaManager>();
        combMan = GetComponent<CombatManager>();
    }    

    public void InitiateBatlle(BattleTypes bt)
    {
        for(int x = 0; x < combatMembers.Count; x++)
        {
            combatMembers[x].SetEquipmentStats();
            if (combatMembers[x].equipmentStats.totalAcuracy == 0)
            {
                AlertMessage.instance.ActivateMessage("You cannot enter a battle without a weapon, make sure each character has a weapon equiped");
                return;
            }
        }
        switch (bt)
        {
            case BattleTypes.travel:
                travelBattle();
                return;
            case BattleTypes.arena:
                return;
            case BattleTypes.boss:
                return;
        }
    }
    public void RespawnCharacters()
    {
        for(int x = 0; x < PlayableCharacters.Length; x++)
        {
            PlayableCharacters[x].HealCharacter();
            localMan.GoToLocalArea(LocalArea.Tavern);
        }
        AlertMessage.instance.ActivateMessage("You got knocked out, the will of the elementals returned you to saftey and restored your vitality");
    }
    public EnemyCharacter ReturnEnemyType(string type, EnemyCharacter[] arrayOfEnemies )
    {
        for(int x = 0; x < arrayOfEnemies.Length; x++)
        {
            if(arrayOfEnemies[x].Name == type)
            {
                return (arrayOfEnemies[x]);
            }
        }
        return null;
    }
    public void travelBattle()
    {
        int ranNum = Random.Range(1, 3);
        EnemyCharacter enemyType;
        if (ranNum < 3)
        {
            enemyType = ReturnEnemyType("Harpie", TravelEnemyTypes);
            combMan.InitiateCombat(combatMembers, enemyType, 1, worldAreaMan.MoveBetweenAfterBattle);
        }
        else if (ranNum < 10)
        {
            enemyType = ReturnEnemyType("Bandit", TravelEnemyTypes);
            combMan.InitiateCombat(combatMembers, enemyType, 5, worldAreaMan.MoveBetweenAfterBattle);
        }
        else
        {
            enemyType = ReturnEnemyType("Wolf", TravelEnemyTypes);
            combMan.InitiateCombat(combatMembers, enemyType, 5, worldAreaMan.MoveBetweenAfterBattle);
        }
    }
    public void ArenaBattle()
    {

    }

}