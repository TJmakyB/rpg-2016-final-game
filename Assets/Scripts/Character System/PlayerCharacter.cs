﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerCharacter : CharacterBase
{


    [SerializeField] private float targetLevelExp;
    [SerializeField] private float currentExp;

    public void GainExperience(float amount)
    {
        currentExp += amount;
        if(currentExp >= targetLevelExp)
        {
            LevelUp();
            targetLevelExp += 2 * targetLevelExp;
        }
    }

    public void Initialize()
    {
        currentValues = StartingValues;
    }
    private void LevelUp()
    {
        currentValues.level++;
        maxValues = maxValues + incrementsPerLevel;
        HealCharacter();
    }
    public void HealCharacter()
    {
        int curLevel = currentValues.level;
        currentValues.health = maxValues.health;
        currentValues.level = curLevel;
        isDead = false;
    }

    public Characters retCharac() { return thisCharacter; }

    public void SetEquipmentStats()
    {
        PlayerInventory PI = InventoryController.instance.playerInven;
        WeaponAndArmourStats tempStats = new WeaponAndArmourStats();
        for(EquipSlots x = 0; x <= EquipSlots.Accessory; x++ )
        {
            if (PI.retEquipment(x, thisCharacter) == null)
                continue;
            equipmentStats = new WeaponAndArmourStats();
            Item tempItem = PI.retEquipment(x, thisCharacter);
            tempStats.totalDamage += tempItem.retDamage();
            tempStats.totalArmour += tempItem.retArmour();
            tempStats.totalAcuracy += tempItem.retAccuracy();
            tempStats.totalMobility += tempItem.retMobility();
        }
        equipmentStats = tempStats;
    }


}


