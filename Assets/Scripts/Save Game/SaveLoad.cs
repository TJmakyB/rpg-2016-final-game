﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad
{

    // function called to save the game at the specific point in the list
    public static List<GameData> savedGames = new List<GameData>();


    public static void SaveGame(int pos)
    { 
        //if (savedGames == null)
        //{
        //    savedGames.Add(GameData.current);

        //}
        //if(savedGames[pos] == null)
        //{
        //    savedGames.Add(GameData.current);
        //}
        //else
        //{
        //    //overwrites current saved game
        //    savedGames[pos] = GameData.current;
        //} 
        // creates the binary formater used to serialize the classes
        BinaryFormatter bf = new BinaryFormatter();
        // the location for the saved games
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, savedGames);
        file.Close();
    }

            //loads the file and sets the GameData list to be the information from the file
    public static void LoadGames(int pos)
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            savedGames = (List<GameData>)bf.Deserialize(file);
            file.Close();
        }
    }
    // returns the list of saved games
    public static List<GameData> RetSavedGames()
    {
        return savedGames;
    }

}