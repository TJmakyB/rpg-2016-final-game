﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class EnemyCharacter : CharacterBase
{

    public float experienceGained;
    public int currentLevel;
    public int numEnemies;

    public EnemyCharacter()
    {
    }

    public EnemyCharacter(EnemyCharacter charac)
    {
        thisCharacter = charac.thisCharacter;
        StartingValues = charac.StartingValues;
        currentValues = charac.currentValues;
        maxValues = charac.maxValues;
        incrementsPerLevel = charac.incrementsPerLevel;
        Name = charac.Name;
        isDead = charac.isDead;
        equipmentStats = charac.equipmentStats;
        experienceGained = charac.experienceGained;
        CurrentSkills = charac.CurrentSkills;
    }

    public virtual void Initialise()
    {
        SetupCharacterStats();
    }
    public virtual void EnemyTurn(CombatManager CM)
    {
        CM.StartCoroutine(GenericAttack(CM));
    }

    protected IEnumerator GenericAttack(CombatManager CM)
    {
        CharacterBase target = PickRandomTarget(CM);
        yield return new WaitForSeconds(CM.enemyTurnlength);        
        CM.AttackCharacter(this, ref target, PickRandomSkill(), true);
    }

    private CharacterBase PickRandomTarget( CombatManager CM)
    {
        List<CharacterBase> AlivePlayers = new List<CharacterBase>();
        for (int x = 0; x < CM.PlayerParticipants.Count; x++)
        {
            if (!CM.PlayerParticipants[x].isDead)
            {
                AlivePlayers.Add(CM.PlayerParticipants[x]);
            }
        }
        int randTarget = Random.Range(0, AlivePlayers.Count);
        return (AlivePlayers[randTarget]);
    }
    private BaseSkill PickRandomSkill()
    {
        int randAbility = Random.Range(0, CurrentSkills.Count);
        return CurrentSkills[randAbility];
    }
    private void SetupCharacterStats()
    {
        maxValues = StartingValues + incrementsPerLevel * currentLevel;
        currentValues = maxValues;
        equipmentStats = equipmentStats * currentLevel;
        experienceGained *= currentLevel;
    }
}