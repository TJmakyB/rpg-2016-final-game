﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ArenaUIController : MonoBehaviour
{
    private class CombatSlots
    {
        public GameObject UISlot;
        public Slider healthBar;
        public Image spriteSlot;
        public Participants character;
        public CombatSprites combatSprites;
    }
    [System.Serializable]
    public class CombatSprites
    {
        public string name;
        public Characters thisCharacter;
        public Sprite idleSprite;
        public Sprite attackSprite;
        public Sprite damageSprite;
    }
    [System.Serializable]
    public class SkillElements
    {
        public Text skillNameTxt;
        public Text DescriptionTxt;
    }

    public SkillElements skillUI;
    public GameObject CombatPanelObj;
    public GameObject AttackFrameObj;
    public GameObject[] EnemyCharacterSlots;
    public GameObject[] PlayerCharacterSlots;
    public CombatSprites[] CharacterSprites;
    [Header("Attack Frame Objects")]
    public float AttackFrameDuration = 2;
    public Image rightCharacterImg;
    public Image leftCharacterImg;
    public Text DamageText;
    public Text debufDamgaeText;
    public Text debufMobilityText;
    public Text debufAccuracyText;
    [Header("Win or Lose")]
    public Text WinLoseText;
    public float closeDuration = 2;
    public BaseSkill currentSkill;
    [Header("Arena Round Counter")]
    public Text RoundNumber;

    private CharacterManager characMan;
    private List<CombatSlots> activeSlots = new List<CombatSlots>();
    private ArenaManager AM;

    private void Start()
    {
        AM = GetComponent<ArenaManager>();
        characMan = GetComponent<CharacterManager>();
    }
    private CombatSlots SetupSlot(CharacterBase CB, GameObject slot, Participants p)
    {
        CombatSlots temp = new CombatSlots();
        temp.UISlot = slot;
        temp.character = p;
        temp.spriteSlot = temp.UISlot.GetComponentsInChildren<Image>()[2];
        temp.combatSprites = SearchForSprite(CB.thisCharacter);
        temp.healthBar = temp.UISlot.GetComponentInChildren<Slider>();
        temp.UISlot.SetActive(true);
        temp.healthBar.maxValue = CB.retMaxHealth();
        temp.healthBar.value = CB.retCurrentHealth();
        temp.spriteSlot.sprite = temp.combatSprites.idleSprite;
        return temp;
    }
    public void SetupCombatUI(List<CharacterBase> Players, List<CharacterBase> Enemies)
    {
        CombatPanelObj.SetActive(true);
        for (int x = 0; x < Players.Count; x++)
        {
            CombatSlots temp = new CombatSlots();
            temp = SetupSlot(Players[x], PlayerCharacterSlots[x], (Participants)x);
            activeSlots.Add(temp);
        }
        Participants enemVal = Participants.enemy1;
        for (int y = 0; y < Enemies.Count; y++, enemVal++)
        {
            CombatSlots temp = new CombatSlots();
            temp = SetupSlot(Enemies[y], EnemyCharacterSlots[y], enemVal);
            activeSlots.Add(temp);
        }
        WinLoseText.text = "";
    }
    public void CloseCombatUI()
    {
        for (int x = 0; x < activeSlots.Count; x++)
        {
            activeSlots[x].UISlot.SetActive(false);
        }
        activeSlots.Clear();
        CombatPanelObj.SetActive(false);
    }
    public void DisplayAttackFrame(Participants attacking, Participants defending, int damage, bool isHit)
    {
        StartCoroutine(AttackFrame(attacking, defending, damage, isHit));
    }
    public void UpdateHealthBars(Participants p, float currentHealth)
    {
        SearchForSlot(p).healthBar.value = currentHealth;
    }
    private CombatSprites SearchForSprite(Characters ch)
    {
        CombatSprites temp = new CombatSprites();
        temp.thisCharacter = ch;
        int pos = Utilities.BruteForceSearch(CharacterSprites, temp, delegate (CombatSprites obj1, CombatSprites obj2)
        {
            if (obj1.thisCharacter == obj2.thisCharacter)
            {
                return true;
            }
            return false;
        });
        return CharacterSprites[pos];
    }
    private IEnumerator AttackFrame(Participants attacking, Participants defending, int damage, bool isHit)
    {

        CombatSlots attackCharSlot = SearchForSlot(attacking);
        CombatSlots defendingCharSlot = SearchForSlot(defending);
        AttackFrameObj.SetActive(true);
        if (attacking < Participants.enemy1)
        {
            leftCharacterImg.sprite = attackCharSlot.combatSprites.attackSprite;
            rightCharacterImg.sprite = defendingCharSlot.combatSprites.damageSprite;
        }
        else
        {
            rightCharacterImg.sprite = attackCharSlot.combatSprites.damageSprite;
            leftCharacterImg.sprite = defendingCharSlot.combatSprites.attackSprite;
        }
        if (isHit)
        {
            for (int x = 0; x < currentSkill.SV.Length; x++)
            {
                CharacterBase temp = AM.returnCurrentCharacter();
                float tempVal = temp.returnCurrentValues().damage * currentSkill.SV[x].multiplier;
                DisplaySkillEffects(currentSkill.SV[x].targetAtribute, Mathf.RoundToInt(tempVal));
                if (currentSkill.SV[x].targetAtribute == Attribute.Health)
                {
                    damage += Mathf.RoundToInt(tempVal);
                }
            }
            DamageText.text = damage.ToString() + " DAMAGE!";
        }

        else
            DamageText.text = "Attack Missed!";
        yield return new WaitForSeconds(AttackFrameDuration);
        debufDamgaeText.gameObject.SetActive(false);
        debufAccuracyText.gameObject.SetActive(false);
        debufMobilityText.gameObject.SetActive(false);
        AttackFrameObj.SetActive(false);
        AM.isAttackFinished(true);
    }

    public void DisplaySkillEffects(Attribute a, int value)
    {
        switch (a)
        {
            case Attribute.Damage:
                debufDamgaeText.gameObject.SetActive(true);
                debufDamgaeText.text = value.ToString() + " DAMAGE REDUCED!";
                break;
            case Attribute.Acuracy:
                debufAccuracyText.gameObject.SetActive(true);
                debufAccuracyText.text = value.ToString() + " ACCURACY REDUCED!";
                break;
            case Attribute.Mobility:
                debufMobilityText.gameObject.SetActive(true);
                debufMobilityText.text = value.ToString() + "MOBILITY REDUCED!";
                break;
        }

    }
    private CombatSlots SearchForSlot(Participants p)
    {
        CombatSlots temp = new CombatSlots();
        temp.character = p;
        int pos = Utilities.BruteForceSearch(activeSlots, temp, delegate (CombatSlots A, CombatSlots B)
        {
            if (A.character == B.character)
            {
                return true;
            }
            else
                return false;
        });
        if (pos == -1)
            return null;
        return activeSlots[pos];
    }

    public void EndCombat(bool isWon)
    {
        if (isWon)
        {
            WinLoseText.text = "You defeated your opponents... Get Ready for Round " + AM.ArenaRoundNum;
        }
        else
        {
            WinLoseText.text = "Your party was knocked out";
        }
        StartCoroutine(EndCombatCoroutine(isWon));
    }

    public void ChangeCurrentSkill(int inc)
    {
        AM.ChangeCurrentSkill(inc);
        currentSkill = AM.currentSkill;
        DisplaySkill();
    }

    public void DisplaySkill()
    {
        skillUI.skillNameTxt.text = currentSkill.skillName;
    }

    private IEnumerator EndCombatCoroutine(bool isWon)
    {
        yield return new WaitForSeconds(closeDuration);
        CloseCombatUI();
        if (!isWon)
        {
            characMan.RespawnCharacters();
        }
    }
}