﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ArenaFight : MonoBehaviour
{

    #region Variables and Classes

    public class CombatParticipant
    {
        public CharacterBase character;
        public Participants participant; // the participant for who the character represents, ie player 1
    }

    private StateMachine<Participants> CombatStates = new StateMachine<Participants>();
    private Participants currentParticipant = Participants.NULL;
    private Participants nextParticipant = Participants.player2;
    private bool attackFrameFin = false;

    public float enemyTurnlength;
    public bool fightOver = false;
    public bool playersTurn = false;
    public List<CharacterBase> EnemyParticipants = new List<CharacterBase>(0);
    public List<CharacterBase> PlayerParticipants = new List<CharacterBase>(0);
    public List<CombatParticipant> TotalParticipants = new List<CombatParticipant>(0);
    public CombatUIController CombUI;
    public CombatDelegates EndBattle;
    public delegate void CombatDelegates();
    private List<BaseSkill> ActiveSkillList = new List<BaseSkill>();
    public BaseSkill currentSkill;
    public int currentSkillIndex;
    #endregion

    #region Initialisation Functions

    public void InitiateCombat(List<PlayerCharacter> Players, EnemyCharacter type, int EnemyLevel, CombatDelegates method)
    {
        EndBattle = method;
        fightOver = false;
        EnemyParticipants.Clear();
        PlayerParticipants.Clear();
        TotalParticipants.Clear();
        GenerateArenaPlayers(Players);
        GenerateArenaEnemies(EnemyLevel, type);
        AddPlayersAndEnemiesToTotal();
        currentParticipant = Participants.NULL;
        CombUI.SetupCombatUI(PlayerParticipants, EnemyParticipants);
        NextTurn();
    }

    private void Awake()
    {
        InitialiseStateMachine();
    }

    private void InitialiseStateMachine()
    {
        CombatStates.AddState(Participants.NULL, FuncType.Enter, null);

        CombatStates.AddState(Participants.enemy1, FuncType.Enter, EnemyTurn);
        CombatStates.AddState(Participants.enemy2, FuncType.Enter, EnemyTurn);
        CombatStates.AddState(Participants.enemy3, FuncType.Enter, EnemyTurn);
        CombatStates.AddState(Participants.enemy4, FuncType.Enter, EnemyTurn);
        CombatStates.AddState(Participants.enemy5, FuncType.Enter, EnemyTurn);

        CombatStates.AddState(Participants.player1, FuncType.Enter, CheckForSkills);
        CombatStates.AddState(Participants.player2, FuncType.Enter, CheckForSkills);
        CombatStates.AddState(Participants.player3, FuncType.Enter, CheckForSkills);
        CombatStates.AddState(Participants.enemy1, FuncType.Enter, CheckForSkills);
        CombatStates.AddState(Participants.enemy2, FuncType.Enter, CheckForSkills);
        CombatStates.AddState(Participants.enemy3, FuncType.Enter, CheckForSkills);
        CombatStates.AddState(Participants.enemy4, FuncType.Enter, CheckForSkills);
        CombatStates.AddState(Participants.enemy5, FuncType.Enter, CheckForSkills);



        CombatStates.AddState(Participants.player1, FuncType.Enter, SetPlayersTurnTrue);
        CombatStates.AddState(Participants.player2, FuncType.Enter, SetPlayersTurnTrue);
        CombatStates.AddState(Participants.player3, FuncType.Enter, SetPlayersTurnTrue);

        CombatStates.AddState(Participants.enemy1, FuncType.Enter, SetPlayersTurnFalse);
        CombatStates.AddState(Participants.enemy2, FuncType.Enter, SetPlayersTurnFalse);
        CombatStates.AddState(Participants.enemy3, FuncType.Enter, SetPlayersTurnFalse);

        CombatStates.AddState(Participants.player1, FuncType.Enter, SetCurrentSkill);
        CombatStates.AddState(Participants.player2, FuncType.Enter, SetCurrentSkill);
        CombatStates.AddState(Participants.player3, FuncType.Enter, SetCurrentSkill);

        CombatStates.AddState(Participants.enemy1, FuncType.Enter, SetCurrentSkill);
        CombatStates.AddState(Participants.enemy2, FuncType.Enter, SetCurrentSkill);
        CombatStates.AddState(Participants.enemy3, FuncType.Enter, SetCurrentSkill);
    }

    private void GenerateArenaEnemies(int EnemyLevel, EnemyCharacter type)
    {

        int numEnemies = Random.Range(1, 4);

        for (int x = 0; x < numEnemies; x++)
        {
            int randUpperLimit = Mathf.FloorToInt(Mathf.Sqrt(EnemyLevel));
            int randomLevel = Random.Range(0, randUpperLimit);
            int fiftyPerc = Random.Range(1, 3);
            if (fiftyPerc == 1)
                randomLevel = EnemyLevel - randomLevel;
            else
                randomLevel = EnemyLevel + randomLevel;
            EnemyCharacter temp = new EnemyCharacter(type);
            temp.currentLevel = randomLevel;
            temp.Initialise();
            EnemyParticipants.Add(temp);
        }

    }
    private void GenerateArenaPlayers(List<PlayerCharacter> players)
    {
        for (int x = 0; x < players.Count; x++)
        {
            PlayerParticipants.Add(players[x]);
        }
    }
    private void AddPlayersAndEnemiesToTotal()
    {
        Participants y = Participants.player1;
        for (int x = (int)Participants.player1; x < PlayerParticipants.Count && y < Participants.enemy1; y++, x++)
        {
            CombatParticipant temp = new CombatParticipant();
            temp.character = PlayerParticipants[x];
            temp.participant = (Participants)x;
            TotalParticipants.Add(temp);
        }
        y = Participants.enemy1;
        for (int x = 0; x < EnemyParticipants.Count && y < Participants.NULL; x++, y++)
        {
            CombatParticipant temp = new CombatParticipant();
            temp.character = EnemyParticipants[x];
            temp.participant = y;
            TotalParticipants.Add(temp);
        }
    }
    private void CheckForSkills()
    {
        ActiveSkillList.Clear();
        CharacterBase currentCharc = TotalParticipants[CurrentParticipantPosition()].character;
        for (int x = 0; x < currentCharc.CurrentSkills.Count; x++)
        {
            BaseSkill tempSkill = new BaseSkill(currentCharc.CurrentSkills[x]);
            ActiveSkillList.Add(tempSkill);
        }
    }
    private void SetCurrentSkill()
    {
        currentSkill = ActiveSkillList[0];
        currentSkillIndex = 0;
        CombUI.currentSkill = currentSkill;
        if (playersTurn)
            CombUI.DisplaySkill();
    }

    #endregion

    #region Arena Functions

    #endregion
   
    #region End Of Battle Functions

    private void PlayersLost()
    {
        fightOver = true;
        CombUI.EndCombat(false);
    }
    private void PlayersWon()
    {
        fightOver = true;
        CombUI.EndCombat(true);
        BattleExperience();
        EndBattle();
    }
    private void BattleExperience()
    {
        float amount = 0;
        for (int y = 0; y < EnemyParticipants.Count; y++)
        {
            amount += ((EnemyCharacter)EnemyParticipants[y]).experienceGained;
        }
        for (int x = 0; x < PlayerParticipants.Count; x++)
        {
            ((PlayerCharacter)PlayerParticipants[x]).GainExperience(amount);
        }
    }
    private void CheckForBatlleOver()
    {
        if (fightOver)
            return;

        int numDeadPlayers = 0;
        int numDeadEnemies = 0;
        for (int x = 0; x < PlayerParticipants.Count; x++)
        {
            if (PlayerParticipants[x].isDead)
            {
                numDeadPlayers++;
            }
        }
        if (numDeadPlayers == PlayerParticipants.Count)
        {
            PlayersLost();
            return;
        }
        for (int y = 0; y < EnemyParticipants.Count; y++)
        {
            if (EnemyParticipants[y].isDead)
            {
                numDeadEnemies++;
            }
        }
        if (numDeadEnemies == EnemyParticipants.Count)
        {
            PlayersWon();
        }
    }

    #endregion

    #region Turn Functions

    private void Update()
    {
        if (attackFrameFin && !fightOver)
        {
            attackFrameFin = false;
            NextTurn();
        }
    }
    public void NextTurn()
    {
        CheckForBatlleOver();
        CheckForNextActiveParticipant();
        currentParticipant = nextParticipant;
        Debug.Log(currentParticipant);
        CombatStates.TransitionToState(currentParticipant);
    }
    private void CheckForNextActiveParticipant()
    {

        if (fightOver)
        {
            nextParticipant = Participants.NULL;
            return;
        }

        int valueToSearchFrom = CurrentParticipantPosition() + 1;
        int valueToSearchTo = TotalParticipants.Count;
        if (valueToSearchFrom >= valueToSearchTo)
        {
            valueToSearchTo = valueToSearchFrom;
            valueToSearchFrom = 0;
        }
        for (int x = valueToSearchFrom; x < valueToSearchTo; x++)
        {
            if (!TotalParticipants[x].character.isDead)
            {
                nextParticipant = TotalParticipants[x].participant;
                return;
            }
            if (x == valueToSearchTo - 1)
            {
                x = -1;
                valueToSearchTo = valueToSearchFrom - 1;
            }
        }
    }
    private void SetPlayersTurnTrue() { playersTurn = true; }
    private void SetPlayersTurnFalse() { playersTurn = false; }

    public void ChangeCurrentSkill(int inc)
    {

        currentSkillIndex += inc;
        currentSkillIndex = (int)Mathf.Repeat(currentSkillIndex, ActiveSkillList.Count);
        currentSkill = ActiveSkillList[currentSkillIndex];
    }
    #endregion

    #region Combat Functions

    public void PlayerAttack(Participants tar)
    {
        CharacterBase player = TotalParticipants[CurrentParticipantPosition()].character;
        CharacterBase target = TotalParticipants[ParticipantPosition(tar)].character;
        if (!target.isDead)
        {
            AttackCharacter(player, ref target, currentSkill);

        }
        else
        {
            AlertMessage.instance.ActivateMessage("That Character is Knocked Out");
        }

    }

    public void AttackCharacter(CharacterBase characterAttacking, ref CharacterBase target, BaseSkill skill)
    {
        bool attackHit = HitChanceCalcualtions(target, characterAttacking);
        Participants attackPartic = CharacterToParticipantSearch(characterAttacking);
        Participants targPartic = CharacterToParticipantSearch(target);
        if (attackHit)
        {
            float weapDamage = 0;
            float weaponDamage = characterAttacking.equipmentStats.totalDamage;
            float totalArmour = target.equipmentStats.totalArmour;
            weapDamage = DamageCalculation(weaponDamage, totalArmour);
            DealDamage(ref target, weapDamage, targPartic, skill, characterAttacking);
            skill.ApplySkilltoTarget(target, characterAttacking);
            CombUI.DisplayAttackFrame(attackPartic, targPartic, Mathf.RoundToInt(weapDamage), true);
        }
        else
            CombUI.DisplayAttackFrame(attackPartic, targPartic, 0, false);
    }

    // returns true if the target has been hit
    private bool HitChanceCalcualtions(CharacterBase target, CharacterBase attacker)
    {
        float acuracy = attacker.equipmentStats.totalAcuracy;
        float mobility = target.equipmentStats.totalMobility;
        float totalVal = mobility + acuracy;
        float hitPerc = acuracy / totalVal * 100f;
        int hitChance = Random.Range(1, 101);
        if (hitChance <= hitPerc)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    private float DamageCalculation(float weaponDamage, float totalArmour)
    {
        float damage = 0;
        float sum = totalArmour + weaponDamage;
        damage = weaponDamage * (1 - totalArmour / sum);
        return damage;
    }
    private void DealDamage(ref CharacterBase target, float totalDamage, Participants tarParticipant, BaseSkill bs, CharacterBase attackingChar)
    {
        target.AdjustTargetAttribute(Attribute.Health, -totalDamage);
        bs.ApplySkilltoTarget(target, attackingChar);
        CombUI.UpdateHealthBars(tarParticipant, target.retCurrentHealth());
        if (target.retCurrentHealth() <= 0)
        {
            target.isDead = true;
        }
    }

    private void EnemyTurn()
    {
        if (!fightOver || !TotalParticipants[(int)currentParticipant].character.isDead)
            StartCoroutine(EnemyTurnCoroutine());
    }

    private IEnumerator EnemyTurnCoroutine()
    {
        List<CharacterBase> AlivePlayers = new List<CharacterBase>();
        for (int x = 0; x < PlayerParticipants.Count; x++)
        {
            if (!PlayerParticipants[x].isDead)
            {
                AlivePlayers.Add(PlayerParticipants[x]);
            }
        }

        int randTarget = Random.Range(0, AlivePlayers.Count);
        CharacterBase target = AlivePlayers[randTarget];
        yield return new WaitForSeconds(enemyTurnlength);
        CharacterBase thisCharac = TotalParticipants[CurrentParticipantPosition()].character;
        int randAbility = Random.Range(0, ActiveSkillList.Count);
        AttackCharacter(thisCharac, ref target, ActiveSkillList[randAbility]);
    }

    #endregion
        
    #region General Funtions

    public CharacterBase returnCurrentCharacter()
    {
        int pos = CurrentParticipantPosition();
        return TotalParticipants[pos].character;
    }
    private int CurrentParticipantPosition()
    {
        return ParticipantPosition(currentParticipant);
    }
    private int ParticipantPosition(Participants searchFor)
    {
        CombatParticipant temp = new CombatParticipant();
        temp.participant = searchFor;
        return Utilities.BruteForceSearch(TotalParticipants, temp, delegate (CombatParticipant obj1, CombatParticipant obj2)
        {
            if (obj1.participant == obj2.participant)
                return true;
            else
                return false;
        });
    }
    private Participants CharacterToParticipantSearch(CharacterBase Char)
    {
        CombatParticipant temp = new CombatParticipant();
        temp.character = Char;
        int pos = Utilities.BruteForceSearch(TotalParticipants, temp, delegate (CombatParticipant a, CombatParticipant b)
        {
            if (a.character == b.character)
            {
                return true;
            }
            else
                return false;
        });
        return TotalParticipants[pos].participant;
    }
    public void isAttackFinished(bool b) { attackFrameFin = b; }
    #endregion

}
