﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class StoreInventory
{
    public List<Item> storeInven = new List<Item>();
    private PlayerInventory PI;
    public float percReducUsedItem = 0.6f;

    public void Initialize()
    {
        ResetInventory();
        PI = InventoryController.instance.playerInven;
    }
    /// <summary>
    /// Adjusts the players gold, inventory and stores active inventory accordinly 
    /// </summary>
    /// <param name="itm">the item that is being purchased</param>
    public void PlayerPurchaseItem(Item itm)
    {
        if(PI.retGold() >= itm.cost)
        {
            if (!PI.isFull())
            {
                PI.AdjustGold(-itm.cost);
                PI.AddPlayerInven(itm);
                AlertMessage.instance.ActivateMessage("You brought " + itm.name);
            }
            else
            {
                AlertMessage.instance.ActivateMessage("Your inventory is too full");
            }
        }
        else
        {
            AlertMessage.instance.ActivateMessage("You do not have enough Drengals");
        }
    }    
    /// <summary>
    /// Sells an item to the store and adjusts the players gold by a reduced item value
    /// </summary>
    /// <param name="itm">Item being sold</param>
    public void PlayerSellItem(Item itm)
    {
        if(itm != null)
        {
            PI.AdjustGold((int)(itm.cost * (1 - percReducUsedItem)));
            PI.RemoveItem(itm);
        }
    }
    /// <summary>
    /// Resets the inventory to the default set inventory
    /// </summary>
    public void ResetInventory()
    {
    }
}
