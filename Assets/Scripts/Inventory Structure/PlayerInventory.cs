﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum QuestItems { EyeOfGraeae, NeameanLionPelt, goldenFleece,wingsOfTheHarpie }

[System.Serializable]
public class PlayerInventory
{

    #region Variables & initialization


    private Inventory inven = new Inventory(50);
    private int gold;
    public Characters currChar = Characters.Deneva;

    public List<QuestItems> QuestItemInven;
    [SerializeField] private List<CharacterEquipment> charEquip = new List<CharacterEquipment>();
    public Weapon[] StartingWeapons;
    public Armour[] StartingArmour;
    public bool[] tutorialHasItem;

    public void Initialize()
    {
        InitCharEquip();
        QuestItemInven.Add(QuestItems.wingsOfTheHarpie);
    }
    private void InitCharEquip()
    {
        CharacterEquipment Deneva = new CharacterEquipment(WeaponType.sword);
        CharacterEquipment Delvon = new CharacterEquipment(WeaponType.staff);
        CharacterEquipment Aurora = new CharacterEquipment(WeaponType.gem);
        charEquip.Add(Deneva);
        charEquip.Add(Aurora);
        charEquip.Add(Delvon);

    }

    public bool DoesPlayerHaveQuestItem(QuestItems item)
    {
        if (Utilities.BruteForceSearch(QuestItemInven, item,
            delegate (QuestItems a, QuestItems b)
            {
                if (a == b)
                    return true;
                else
                    return false;
            }) != -1)
            return true;
        else
            return false;
    }
    public void EquipArenaTutorialItems()
    {
        for (int x = 0; x < charEquip.Count; x++)
        {
            currChar = (Characters)x;
            inven.Add(StartingWeapons[x]);
            inven.Add(StartingArmour[x]);
            SetEquipment(StartingWeapons[x]);
            SetEquipment(StartingArmour[x]);
        }
    }
    public void UnequipArenaTutorialItems()
    {
        currChar = Characters.Deneva;
        RemoveEquipment(EquipSlots.Weapon);
        RemoveEquipment(EquipSlots.Armour_Body);
        inven.Remove(StartingWeapons[0]);
        inven.Remove(StartingArmour[0]);
    }

    #endregion

    #region Test Functions and variables

    //Prints part of the inventory for testing 
    public void printInven()
    {
        List<ItemSpace> curInv = inven.retInv();
        for(int x = 0; x < curInv.Count; x++)
        {
            Debug.Log("You have " + curInv[x].NumberOfItem + " of The " + curInv[x].ItemSlot.name);
        }
    }
    #endregion

    #region Equipment & Inventory Management
    /// <summary>
    /// Sets the players Equiped items with an item that must be from the players inventory
    /// </summary>
    /// <param name="itm">New item to be equiped</param>
    /// <param name="type">The type of item that is being equiped eg. weapon, shield etc.</param>
    public void SetEquipment(Item itm)
    {        
        if (itm != null)
        {
            EquipSlots type = itm.type;
            switch (type)
            {
                case EquipSlots.Weapon:
                    EquipItem((Weapon)itm, ref charEquip[(int)currChar].weapon);
                    return;
                case EquipSlots.Armour_Body:
                        EquipItem<Armour>((Armour)itm, ref charEquip[(int)currChar].body);
                    return;
                case EquipSlots.Armour_Helmet:                
                        EquipItem<Armour>((Armour)itm, ref charEquip[(int)currChar].head);
                        return;
                default:
                    return;

            }
        }
        
    }
    /// <summary>
    /// returns the specific equipment item i.e. an equiped piece of armour
    /// </summary>
    /// <param name="eqt">The Equipment to return</param>
    /// <returns></returns>
    public Item retEquipment(EquipSlots eqt, Characters Char)
    {
        switch (eqt)
        {
            case EquipSlots.Weapon:
                return charEquip[(int)Char].weapon;
            case EquipSlots.Accessory:
                return charEquip[(int)Char].blessing;
            case EquipSlots.Armour_Helmet:
                return charEquip[(int)Char].head;
            case EquipSlots.Armour_Body:
                return charEquip[(int)Char].body;
            default:
                return null;

        }


    }
    /// <summary>
    /// Removes a specific item of equipment and places it into the inventory if the inventory is not full
    /// </summary>
    /// <param name="type">The particular item that is being removed</param>
    /// <param name="AT">The specific piece of armour to be removed</param>
    public void RemoveEquipment(EquipSlots type)
    {
        switch (type)
        {
            case EquipSlots.Weapon:
                Unequip<Weapon>(ref charEquip[(int)currChar].weapon);
                return;
            case EquipSlots.Accessory:
                Unequip<Item>(ref charEquip[(int)currChar].blessing);
                return;
            case EquipSlots.Armour_Helmet:
                Unequip<Armour>(ref charEquip[(int)currChar].head);
                return;
            case EquipSlots.Armour_Body:
                Unequip<Armour>(ref charEquip[(int)currChar].body);
                return;
        }
        
    }
    /// <summary>
    /// Discards the a particular item from the inventory
    /// </summary>
    /// <param name="itm">The item that is discarded</param>
    public void RemoveItem(Item itm)
    {
        inven.Remove(itm);
    }
    /// <summary>
    /// Add an item to the players inventory
    /// </summary>
    /// <param name="itm">Item to be added</param>
    public void AddPlayerInven(Item itm)
    {
        if(!inven.isInvenFull())
        {
            inven.Add(itm);
        }
    }

    // outputs a message to the console that the inventory is full (Tempory)
    private void InvenFullMessage()
    {
        AlertMessage.instance.ActivateMessage("You're inventory is full discard or sell some items");
    }

    // returns the item to be equiped if it is in the inventory and the type is correct
    private void EquipItem<T>( T newItem, ref T curItem) where T:Item
    {
        if (inven.inInventory(newItem))
        {
            //if the player has an item currently equiped
            if (curItem != null)
            {
                // removes the new equipment from the inventory
                inven.Remove(newItem);
                // adds the old equipment to the inventory
                inven.Add(curItem);
            }
            else
            {
                //removes the new equipment from the inventory
                inven.Remove(newItem);
            }
            curItem = newItem;
            
        }
    }

    // Unequips a particualr item and places it into the inventory
    private void Unequip<T>(ref T itm ) where T:Item
    {
        if (!inven.isInvenFull())
        {
            inven.Add(itm);
            itm = null;
        }
        else
        {
            InvenFullMessage();
        }


    }

    /// <summary>
    /// returns if the players inventory is full
    /// </summary>
    /// <returns></returns>
    public bool isFull()
    {
        return (inven.isInvenFull());
    }
    public Inventory retInven()
    {
        return inven;
    }
    public bool isWeaponPrefered(Weapon weap)
    {
        
        return charEquip[(int)currChar].preferedWeapon == weap.weaponType;
    }
    #endregion

    #region Gold Management
    // returns the players amount of gold
    public int retGold() { return gold; }
    /// <summary>
    /// Adjusts the amount of gold that the player owns
    /// </summary>
    /// <param name="val"> value that is added </param>
    public void AdjustGold(int val) { gold += val; }
    #endregion
     
}

