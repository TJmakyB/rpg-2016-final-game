﻿/*
script description:
*/
using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterEquipment 
{
    //Equipment items and possible slots
    public Weapon weapon;
    public Item blessing;
    public Armour head;
    public Armour body;
    public WeaponType preferedWeapon;

    /// <summary>
    /// input the prefered weapon that the character desires
    /// </summary>
    /// <param name="pW">Characters Prefered Weapon Type</param>
    public CharacterEquipment(WeaponType pW)
    {
        preferedWeapon = pW;
    }

}