﻿/*
script description:
*/
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    #region Variables and Classes
    [System.Serializable]
    public class UI_ItemElements
    {
        public Text nameTxt;
        public Text Cost;
        public Text armAndDam;
        public Text acur;
        public Text durab;
        public Text type;
        public Text Rarity;
        public Text weight;
        public Sprite image;
        public Text NumberOfItem;
        public Text PlayerGold;
    }
    public bool finalBattle;
    public GameController GameCon;
    public static int curSI = 0; // current store Item
    public static InventoryController instance;
    public PlayerInventory playerInven = new PlayerInventory();
    [Header("UI Element Containers")]
    public UI_ItemElements PE = new UI_ItemElements();
    public UI_ItemElements SE = new UI_ItemElements();
    [Header("Store Inventories")]
    public StoreInventory[] CityStoreInvens;
    public WorldLocation curLocation;
    public RectTransform itemDesc;
    public Vector3 descripOffset;
    public GameObject UI_Item_Prefab;
    public RectTransform scrollview_Content;
    public Vector3 startingItemPos;
    public GameObject[] EquipmentSlotsUI;
    public float descripBoxMinY;
    public bool canDisplayItemDescrip = true;
    public Item DefaultItem;
    public Text CharacterNameText;
    public bool EquipmentTutorial = false;

    [Header("Inventory Pannels")]
    public GameObject StoreInvenGameObject;
    public GameObject PlayerInvenGameObject;

    private GameObject currentInventoryElement = null;
    private List<ItemSpace> weaponList = new List<ItemSpace>(0);
    private List<ItemSpace> armourList = new List<ItemSpace>(0);
    private List<GameObject> currentPlayerDisp = new List<GameObject>(0);
    private int currentTab = 0;
    private CharacterManager characCon;
    public bool[] tutorialHasItem = new bool[] { false, false };
    public bool tutorial = true;

    #endregion

    void Awake()
    {
        instance = this;
        playerInven.Initialize();
        playerInven.AdjustGold(+200);
    }

    void Start()
    {
        characCon = GetComponent<CharacterManager>();

    }
    public void DisplayItemUIElements( UI_ItemElements elem, Item curItem)
    {
        elem.nameTxt.text = curItem.name;
        if(elem.Cost != null)
            elem.Cost.text = curItem.cost.ToString() + " Drengals";
        elem.durab.text = "Durability: " + curItem.retDurability();
        elem.weight.text = "Weight: " + curItem.retMobility();
        if(elem.type != null)
            elem.type.text = ItemTypeToString(curItem);
        if (curItem.type == EquipSlots.Weapon)
        {
            elem.armAndDam.text = "Damage: " + curItem.retDamage();
            elem.acur.text = "Accuracy: " + curItem.retAccuracy();

        }
        else if (curItem.type == EquipSlots.Armour_Body || curItem.type == EquipSlots.Armour_Helmet)
        {
            elem.armAndDam.text = "Armour: " + curItem.retArmour();
            elem.acur.text = "";
        }
    }
    public void DisplayNumberOfItem(UI_ItemElements elem, string num)
    {
        elem.NumberOfItem.text = num;
    }

    #region Store UI System

    public void SetupStoreUI()
    {
        curLocation = WorldAreaManager.currentLocation;
        CityStoreInvens[(int)curLocation].Initialize();
        curLocation = WorldAreaManager.currentLocation;
        StoreInvenGameObject.SetActive(true);
        DisplayItemUIElements(SE, CityStoreInvens[(int)curLocation].storeInven[curSI]);
        DisplayGold();
    }
    public void DisplayGold()
    {
        SE.PlayerGold.text = playerInven.retGold().ToString() + " Drengals";
    }
    public void IncrStoreElement(int inc)
    {
        curSI += inc;
        curSI = Mathf.Clamp(curSI,0, CityStoreInvens[(int)curLocation].storeInven.Count-1);
        DisplayItemUIElements(SE, CityStoreInvens[(int)curLocation].storeInven[curSI]);
    }
    public void PurchaseItem()
    {
        Item curItem = CityStoreInvens[(int)curLocation].storeInven[curSI];
        if(tutorial)
        {
            if(tutorialHasItem[0] && curItem.type == EquipSlots.Weapon)
            {
                AlertMessage.instance.ActivateMessage("I should buy some armour instead of a second weapon");
                return;
            }
            else if(tutorialHasItem[1])
            {
                AlertMessage.instance.ActivateMessage("I should buy a weapon instead of more armour");
                return;
            }
        }
        CityStoreInvens[(int)curLocation].PlayerPurchaseItem(curItem);
        DisplayGold();
        GameCon.PurchaseEvent(curItem);           
    }
    public string ItemTypeToString(Item itm)
    {
        string typename = "";
        switch (itm.type)
        {
            case EquipSlots.Weapon:
                typename = WeaponTypeToString((Weapon)itm);
                return typename;
            case EquipSlots.Armour_Body:
                typename = ArmourTypeToString((Armour)itm, "Body Armour");
                return typename;
            case EquipSlots.Armour_Helmet:
                typename = ArmourTypeToString((Armour)itm, "Helmet");
                return typename;
            case EquipSlots.Accessory:
                return typename;
            default:
                return "";
        }

    }
    string WeaponTypeToString(Weapon weap)
    {
        switch (weap.weaponType)
        {
            case WeaponType.gem:
                return "Magic Gem";
            case WeaponType.staff:
                return "Staff";
            case WeaponType.sword:
                return "Sword";
            default:
                return "";

        }

    }
    string ArmourTypeToString(Armour arm, string equipType)
    {
        switch(arm.armourType)
        {
            case ArmourType.light:
                return equipType + ", Light";
            case ArmourType.medium:
                return equipType + ", Medium";
            case ArmourType.heavy:
                return equipType + ", Heavy";
            default:
                return "";
        }
    }
    string RarityTypeToString(Item itm)
    {
        switch(itm.rarity)
        {
            case Rarity.common:
                return "Common";
            case Rarity.rare:
                return "Rare";
            case Rarity.epic:
                return "Epic";
            case Rarity.legendary:
                return "Legendary";
            default:
                return "";
        }
    }
    #endregion

    #region PlayerInventory UI System

    public void HoverOverItem(GameObject obj)
    {

        ItemSpace hoverItem = null;
        if (obj.tag == "ItemSlot")
            hoverItem = obj.GetComponent<UI_ItemSlot>().thisItem;
        else if (obj.tag == "EquipSlot")
            hoverItem = obj.GetComponent<EquipSlotUI>().EquipedItem;
        if (hoverItem == null)
            return;
        if (currentInventoryElement == obj || canDisplayItemDescrip == false || hoverItem.ItemSlot == null)
            return;

        itemDesc.SetAsLastSibling();
        itemDesc.gameObject.SetActive(true);
        currentInventoryElement = obj;
        DisplayItemUIElements(PE, hoverItem.ItemSlot);
        DisplayNumberOfItem(PE, "Number of: " + hoverItem.NumberOfItem.ToString());
        if (obj.tag == "EquipSlot")
            DisplayNumberOfItem(PE, "");
        Vector3 descripPos = obj.GetComponent<RectTransform>().position;
        descripPos -= descripOffset;
        if (obj.GetComponent<RectTransform>().localPosition.y <= descripBoxMinY)
            descripPos.y = scrollview_Content.TransformPoint(new Vector3(0,descripBoxMinY,0)).y;
        itemDesc.position = descripPos;
    }
    public void ExitHoverOver()
    {
        itemDesc.gameObject.SetActive(false);
        currentInventoryElement = null;
    }

    public void OpenPlayerInventory()
    {
        PlayerInvenGameObject.SetActive(true);
        SeperateInventory();
        SetUpInvenList(weaponList);
        currentTab = 1;
        SetupEquipment();
    }
    void SeperateInventory()
    {
        playerInven.retInven().SortInventoryAlphabetically();
        List<ItemSpace> currPlayInven = playerInven.retInven().retInv();
        weaponList.Clear();
        armourList.Clear();
        for (int x = 0; x < currPlayInven.Count; x++)
        {
            Item itm = currPlayInven[x].ItemSlot;
            switch (itm.type)
            {
                case EquipSlots.Weapon:
                    weaponList.Add(currPlayInven[x]);
                    break;
                case EquipSlots.Armour_Body:
                case EquipSlots.Armour_Helmet:
                    armourList.Add(currPlayInven[x]);
                    break;
                default:
                    break;
            }
        }
    }

    void SetUpInvenList(List<ItemSpace> dispItem)
    {
        Vector3 currRect = startingItemPos;
        int numOfSlots = dispItem.Count;
        for(int x = 0; x < numOfSlots; x++, currRect.y -= 45)
        {
            Item itm = dispItem[x].ItemSlot;
            GameObject newItem = CreateSlots(currRect);
            newItem.GetComponent<UI_ItemSlot>().thisItem = dispItem[x];
            currentPlayerDisp.Add(newItem);
            Image iconImage = newItem.GetComponentInChildren<Image>();
            Text name = newItem.GetComponentInChildren<Text>();
            name.text = itm.name;
        }
    }
    public void DeletePlayerDisp()
    {
        for(int x = 0; x < currentPlayerDisp.Count; x ++)
        {
            Destroy(currentPlayerDisp[x]);
        }
        currentPlayerDisp.Clear();
    }
    public void ChangeTab(int tab)
    {
        if (tab == currentTab)
            return;

        DeletePlayerDisp();
        switch(tab)
        {
            case 1:
                SetUpInvenList(weaponList);
                break;
            case 2:
                SetUpInvenList(armourList);
                break;
        }
        currentTab = tab;
    }
    GameObject CreateSlots(Vector3 currPos)
    {
        GameObject newItem = Instantiate(UI_Item_Prefab, Vector3.zero, Quaternion.identity) as GameObject;
        newItem.transform.SetParent(scrollview_Content, false);
        RectTransform rect = newItem.GetComponent<RectTransform>();
        rect.anchorMax = new Vector2(1, 1);
        rect.anchorMin = new Vector2(1, 1);
        rect.pivot = new Vector2(0.5f, 0.5f);
        rect.anchoredPosition = currPos;
        scrollview_Content.sizeDelta = new Vector2(scrollview_Content.sizeDelta.x, -1*currPos.y + 50);
        scrollview_Content.anchoredPosition = new Vector2(-75, -24);
        return newItem;
    }
    public void EquipItemToSlot(GameObject slot, ItemSpace itmSpace)
    {
        EquipSlotUI eqSlot = slot.GetComponent<EquipSlotUI>();
        Item itm = itmSpace.ItemSlot;
        if(eqSlot.thisSlot == EquipSlots.Weapon && itm.type == EquipSlots.Weapon)
        {
            if (!playerInven.isWeaponPrefered( (Weapon)itm))
            {
                AlertMessage.instance.ActivateMessage("Not the Preffered Weapon");
                return;
            }
        }
        if (eqSlot.thisSlot != itm.type)
        {
            AlertMessage.instance.ActivateMessage("Not The Right Slot");
            return;
        }

        eqSlot.EquipedItem = itmSpace;
        playerInven.SetEquipment(itm);
        ResetInventoryDisplay();
        DisplayEquipment(slot, itm);      
    }
    private void DisplayEquipment(GameObject slot, Item itm)
    {
        Text name = slot.GetComponentInChildren<Text>();
        Image Icon = slot.GetComponentInChildren<Image>();
        name.text = itm.name;
    }
    private void ResetInventoryDisplay()
    {
        DeletePlayerDisp();
        SeperateInventory();
        switch (currentTab)
        {
            case 1:
                SetUpInvenList(weaponList);
                break;
            case 2:
                SetUpInvenList(armourList);
                break;
        }
    }
    private void SetupEquipment()
    {
        DisplayCharacterName(playerInven.currChar);
        for(EquipSlots x = EquipSlots.Weapon; x < (EquipSlots)4; x++)
        {
            if(playerInven.retEquipment(x, playerInven.currChar) != null)
            {
                Item itmTemp = playerInven.retEquipment(x, playerInven.currChar);
                ItemSpace temp = new ItemSpace(itmTemp,1);
                DisplayEquipment(EquipmentSlotsUI[(int)x], itmTemp);
                EquipmentSlotsUI[(int)x].GetComponent<EquipSlotUI>().EquipedItem = temp;
            }
            else
            {
                DisplayEquipment(EquipmentSlotsUI[(int)x], DefaultItem);
                EquipmentSlotsUI[(int)x].GetComponent<EquipSlotUI>().EquipedItem = null;
            }
        }
    }
    public void IncrementToCharacter(int incr)
    {
        int tempChar = (int)playerInven.currChar + incr;
        int maxChar = characCon.PartyMembers.Count -1;
        tempChar = Mathf.Clamp(tempChar, 0, maxChar);
        ChangeEquipmentCharacter((Characters)tempChar);

    }

    private void DisplayCharacterName(Characters current)
    {
        switch (current)
        {
            case Characters.Deneva:
                CharacterNameText.text = "Deneva";
                return;
            case Characters.Delvon:
                CharacterNameText.text = "Delvon";
                return;
            case Characters.Aurora:
                CharacterNameText.text = "Aurora";
                return;
        }

    }
    public void ChangeEquipmentCharacter(Characters newCharacter)
    {
        playerInven.currChar = newCharacter;
        DisplayCharacterName(newCharacter);
        SetupEquipment();
    }

    public void CloseOverlayPannel(Object obj)
    {
        if(EquipmentTutorial)
        {
            if (playerInven.retEquipment(EquipSlots.Weapon, Characters.Deneva) == null 
             || playerInven.retEquipment(EquipSlots.Armour_Body, Characters.Deneva) == null)
            {
                AlertMessage.instance.ActivateMessage("I should Equip some Body Armour and a Weapon before fighting Aurora");
                return;
            }
            else
            {
                characCon.InitiateBatlle(BattleTypes.boss);
                EquipmentTutorial = false;
            }
        }
        if (finalBattle)
        {
            characCon.InitiateBatlle(BattleTypes.boss);
        }
        GameObject gameObj = obj as GameObject;
        gameObj.SetActive(false);
    }
    #endregion
}