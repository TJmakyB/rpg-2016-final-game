﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PannelOverMouse : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler
{
    InventoryController invenCon;

    void Start()
    {
        invenCon = GameObject.Find("SceneController").GetComponent<InventoryController>();
    }
    public void OnPointerEnter(PointerEventData data)
    {
        invenCon.HoverOverItem(gameObject);
    }
    public void OnPointerExit(PointerEventData data)
    {
        invenCon.ExitHoverOver();
    }
}