﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{


    private InventoryController invenCon;
    private Vector2 pointerOffset;
    private Vector3 itemSlotInitalPos; 
    private RectTransform parentTransform;
    private RectTransform canvasRectTransform;
    private RectTransform itemSlotRectTransform;
    private RectTransform inventoryBackgroundTransform;
    private GameObject[] equipSlotGameObjects;

    void Start()
    {
        Canvas canvas = GetComponentInParent<Canvas>();
        invenCon = GameObject.Find("SceneController").GetComponent<InventoryController>();
        if (canvas != null)
        {
            equipSlotGameObjects = GameObject.FindGameObjectsWithTag("EquipSlot");
            inventoryBackgroundTransform = GameObject.Find("PlayerInventoryBackground").transform as RectTransform;
            canvasRectTransform = canvas.transform as RectTransform;
            itemSlotRectTransform = transform as RectTransform;
            parentTransform = transform.parent as RectTransform;
        }
    }
    public void OnPointerDown(PointerEventData data)
    {
        parentTransform = transform.parent as RectTransform;
        itemSlotInitalPos = itemSlotRectTransform.localPosition;
        itemSlotRectTransform.SetParent(canvasRectTransform,true);
        itemSlotRectTransform.SetAsLastSibling();
        invenCon.canDisplayItemDescrip = false;
        invenCon.ExitHoverOver();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(itemSlotRectTransform, data.position, data.pressEventCamera, out pointerOffset);
    }
    public void OnPointerUp(PointerEventData data)
    {
        invenCon.canDisplayItemDescrip = true;
        SnapToInitialPos();
        for(int x = 0; x < equipSlotGameObjects.Length; x++)
        {
            RectTransform temp = equipSlotGameObjects[x].transform as RectTransform;
            if (CheckForEquipSlot(data, temp))
            {
                invenCon.EquipItemToSlot(equipSlotGameObjects[x], gameObject.GetComponent<UI_ItemSlot>().thisItem);
                return;
            }
        }
    }

    public void OnDrag(PointerEventData data)
    {
        if (itemSlotRectTransform == null)
            return;
        
        Vector2 pointerPostion = ClampToWindow(data);
        Vector2 localPointerPosition;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            canvasRectTransform, pointerPostion, data.pressEventCamera, out localPointerPosition
        ))
        {
            itemSlotRectTransform.localPosition = localPointerPosition - pointerOffset;
        }
    }

    private void SnapToInitialPos()
    {
        itemSlotRectTransform.SetParent(parentTransform);
        itemSlotRectTransform.localPosition = itemSlotInitalPos;
    }
    Vector2 ClampToWindow(PointerEventData data)
    {
        Vector2 rawPointerPosition = data.position;

        Vector3[] invenCorners = new Vector3[4];
        inventoryBackgroundTransform.GetWorldCorners(invenCorners);
        for(int x = 0; x < invenCorners.Length; x++)
        {
            invenCorners[x] = Camera.main.WorldToScreenPoint( invenCorners[x]);
        }

        float clampedX = Mathf.Clamp(rawPointerPosition.x, invenCorners[0].x, invenCorners[2].x);
        float clampedY = Mathf.Clamp(rawPointerPosition.y, invenCorners[0].y, invenCorners[2].y);

        Vector2 newPointerPosition = new Vector2(clampedX, clampedY);
        return newPointerPosition;
    }
    bool CheckForEquipSlot(PointerEventData data, RectTransform equipBlock)
    {
        Vector2 pointerPos = data.position;
        Vector3[] blockCorners = new Vector3[4];
        equipBlock.GetWorldCorners(blockCorners);
        for (int x = 0; x <blockCorners.Length; x++)
        {
            blockCorners[x] = Camera.main.WorldToScreenPoint(blockCorners[x]);
        }
        if ((pointerPos.x > blockCorners[0].x && pointerPos.x < blockCorners[2].x)
            && (pointerPos.y > blockCorners[0].y && pointerPos.y < blockCorners[2].y))
            return true;

        return false;
    }
}