﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ItemSpace
{
    public Item ItemSlot;
    public int NumberOfItem;
    public ItemSpace(Item itm, int num)
    {
        ItemSlot = itm;
        NumberOfItem = num;

    }
}

[System.Serializable]
public class Inventory
{

    // allows the inventory list to be defined within the unity editor
    [SerializeField] private List<ItemSpace> curInv = new List<ItemSpace>();
    [SerializeField] private int maxInv = 20;         // max size of the inventory
    private int curNumItems = 0;                     // current number of items in the inventory

    /// <summary>
    /// Default constructor defines max size to 20
    /// </summary>
    public Inventory()
    {
        maxInv = 20;
        curNumItems = 0;
    }
    /// <summary>
    /// Constructor that defines the max size
    /// </summary>
    /// <param name="max">max size for the inventory</param>
    public Inventory(int max)
    {
        maxInv = max;
        curNumItems = 0;
    }
    /// <summary>
    /// Adds an item to the inventory
    /// </summary>
    /// <param name="itm">The item that is added</param>
    public void Add(Item itm)
    {
        if (!isInvenFull() && itm != null)
        {
            // moves through the inventory to check if in item exists
            for (int x = 0; x < curInv.Count; x++)
            {
                if (curInv[x].ItemSlot.name == itm.name)
                {
                    // if the item already exists the number of that item is incremented
                    // rather than adding another object to the list
                    curInv[x].NumberOfItem++;
                    curNumItems++;
                    return;
                }
            }
            // if the item does not exist the object is added to the list
            ItemSpace tempSpace = new ItemSpace(itm, 1);
            curInv.Add(tempSpace);
            
            curNumItems++;
            return;
        }
        else
            AlertMessage.instance.ActivateMessage("Inventory Full");
    }
    /// <summary>
    /// removes an item from the inventory
    /// </summary>
    /// <param name="itm">the item to be removed</param>
    public void Remove(Item itm)
    {
        if (itm != null)
        {
            // moves through the inventory to check if in item exists
            for (int x = 0; x <= curInv.Count; x++)
            {

                if (itm.name == curInv[x].ItemSlot.name) // if item is in the inventory
                {
                    if (curInv[x].NumberOfItem > 1) // if there are multiple items of the same name
                    {
                        // decrements the number of items
                        curInv[x].NumberOfItem--;
                        curNumItems--;
                        return;
                    }
                    else
                    {
                        // removes the item from that specific item from the list
                        curInv.RemoveAt(x);
                        curNumItems--;
                        return;
                    }
                }
            }
        }
    }
    /// <summary>
    /// returns the inventory list
    /// </summary>
    /// <returns></returns>
    public List<ItemSpace> retInv()
    {
        return curInv;
    }
    /// <summary>
    /// returns a boolean to check if an item is in the inventory
    /// </summary>
    /// <param name="itm"></param>
    /// <returns></returns>
    public bool inInventory(Item itm)
    {
        for(int x = 0; x < curInv.Count; x++)
        {
            if(curInv[x].ItemSlot == itm)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// returns true if the inventory is full
    /// </summary>
    /// <returns></returns>
    public bool isInvenFull()
    {
        return maxInv == curNumItems;
    }


    public void SortInventoryAlphabetically()
    {
        curInv.Sort(delegate (ItemSpace slot1, ItemSpace slot2)
        {
            if (slot1 == null && slot2 == null) return 0;
            else if (slot1 == null) return -1;
            else if (slot2 == null) return 1;
            else            
                return slot1.ItemSlot.name.CompareTo(slot2.ItemSlot.name);
            
        });
            
    }
}
