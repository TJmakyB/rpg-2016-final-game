using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Participant{
  public int? hitPoints;
  public int? armourPoints;
  public List<SkillModel> skills;
  public bool isHuman;
  public bool isMyTurn;

  public Participant(CharacterModel player){
	hitPoints = player.GetHP();
	armourPoints = player.GetMobility();// However AP stat is determined
    //skills =
    isHuman = true;
    isMyTurn = false;
  }

	public Participant(int? hp = null, int? ap = null, List<SkillModel> skill=null)
  {
	  hitPoints = hp;
	  armourPoints = ap;
	  isHuman = false;
	  isMyTurn = false;

	  if (skill == null)
   	{
	    skill = new List<SkillModel>();
	  }

	  for (int i=0; i<=3; i++)
	  {
		    SkillModel mySkill = SkillModel.GenerateSkill();
		    skill.Add(mySkill);
	  }

    if (isMyTurn){
			int chosenSkill = Random.Range(0,skill.Count);
      //execute skill
    }


  }
}
