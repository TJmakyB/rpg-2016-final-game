﻿/*
script description:
*/
using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Skills/Basic Skill")]
public class BaseSkill : ScriptableObject
{
    [System.Serializable]
    public class SkillValue
    {
        public Attribute targetAtribute;
        public float multiplier;
        public bool isDamage;
    }

    public string skillName;
    public Characters character;
    public skills skill;
    public bool isBuff;
    public SkillValue[] SV;
    public bool isTrueDamage = true;
    public int TurnCoolDownValue;
    public int turnCooldown;
    public bool isActive = false;
    
    public BaseSkill()
    {

    }

    public BaseSkill( BaseSkill BS)
    {
        skillName = BS.skillName;
        character = BS.character;
        SV = BS.SV;
        isBuff = BS.isBuff;
        isTrueDamage = BS.isTrueDamage;
        TurnCoolDownValue = BS.TurnCoolDownValue;
        isActive = BS.isActive;
    }
    public bool isDamgingSkill()
    {
        for(int x = 0; x < SV.Length; x++)
        {
            if (SV[x].targetAtribute == Attribute.Health)
                return true;
        }
        return false;
    }
    public virtual void ApplySkilltoTarget(CharacterBase target, CharacterBase attackingCharacter)
    {
        for (int x = 0; x < SV.Length; x++)
        {
            float temp = SV[x].multiplier * attackingCharacter.returnCurrentValues().damage;
            if (!isBuff)
                temp *= -1f;
            target.AdjustTargetAttribute(SV[x].targetAtribute, temp);
        }
    }
    public SkillValue retSkillValue(Attribute attri)
    {
        SkillValue temp = new SkillValue();
        temp.targetAtribute = attri;
        int pos = Utilities.BruteForceSearch(SV, temp, delegate (SkillValue A, SkillValue B) 
        { if (A.targetAtribute == B.targetAtribute)
                return true;
            else
                return false;
        });
        if (pos == -1)
            return null;
        else
            return SV[pos];
    }
}