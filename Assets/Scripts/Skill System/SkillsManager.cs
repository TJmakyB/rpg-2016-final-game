﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum skills { s1, s2, s3, s4, s5}
public class SkillsManager : MonoBehaviour
{
    [System.Serializable]
    public class CharacterSkillSets
    {
        public string name;
        public Characters thisCharacter;
        [Header("Character Skill List")]
        public List<BaseSkill> skillList;

        public CharacterSkillSets(CharacterSkillSets CSS)
        {
            name = CSS.name;
            thisCharacter = CSS.thisCharacter;
            for (int x = 0; x < CSS.skillList.Count; x++)
            {
                skillList.Add(new BaseSkill(skillList[x]));
            }            
        }
        public CharacterSkillSets()
        {

        }
    }

    public List<CharacterSkillSets> CharacterSkillsObj= new List<CharacterSkillSets>();
    private List<CharacterSkillSets> characterSkills = new List<CharacterSkillSets>();

    public void InitialiseCharacterSkills()
    {
        characterSkills.Clear();
        for(int x = 0; x < CharacterSkillsObj.Count; x++)
        {
            CharacterSkillSets temp = new CharacterSkillSets(CharacterSkillsObj[x]);
            characterSkills.Add(temp);
        }
    }

    public CharacterSkillSets ReturnCharactersSkills(Characters charac)
    {
        CharacterSkillSets temp = new CharacterSkillSets();
        temp.thisCharacter = charac;
        int pos = Utilities.BruteForceSearch(characterSkills, temp, delegate (CharacterSkillSets A, CharacterSkillSets B) 
        {
            if (A.thisCharacter == B.thisCharacter)
                return true;
            else
                return false;
        });
        return CharacterSkillsObj[pos];
    }

    public void SetSkillActive(skills s, Characters c)
    {
        CharacterSkillSets charac = ReturnCharactersSkills(c);
        BaseSkill temp = SearchForSkills(charac.skillList, s);
        temp.isActive = true;
    }
    public BaseSkill SearchForSkills(List<BaseSkill> skillList, skills skill)
    {
        BaseSkill temp = new BaseSkill();
        temp.skill = skill;
        int pos = Utilities.BruteForceSearch(skillList, temp, delegate (BaseSkill A, BaseSkill B)
        {
            if (A.skill == B.skill)
                return true;
            else
                return false;
        });
        return skillList[pos];
    }
   
}
