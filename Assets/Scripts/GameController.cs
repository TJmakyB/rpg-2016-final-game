﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public string[] StartingItemNames;

    public bool StoreTutorial = false;
    public bool isNextQuest = false;
    public Button QuestButton;
    public Button BossFightButton;
    public GameObject FadeScreen;
    public Weapon Vanquisher;
    private List<NarativeSystem.NarativeDelegates> AfterFight = new List<NarativeSystem.NarativeDelegates>();
    private NarativeSystem narativeSyst;
    private WorldAreaManager worldCon;
    private LocalAreaManager localAreaCon;
    private GameObject sceneCon;
    private CharacterManager characCon;
    private CombatManager combCon;
    private InventoryController invenCon;
    public int CurrentBossFight = 0;
    private delegate void bossDelegate();
    private bool[] PurchasedStartingItems = new bool[] { false, false};

    #region Initialisation
    public void Start()
    {
        sceneCon = GameObject.Find("SceneController");
        narativeSyst = sceneCon.GetComponent<NarativeSystem>();
        worldCon = sceneCon.GetComponent<WorldAreaManager>();
        localAreaCon = sceneCon.GetComponent<LocalAreaManager>();
        characCon = sceneCon.GetComponent<CharacterManager>();
        combCon = sceneCon.GetComponent<CombatManager>();
        invenCon = sceneCon.GetComponent<InventoryController>();
        InitialiseAfterFight();
    }
    private void InitialiseAfterFight()
    {

        for(int x = 0; x < characCon.BossEnemyTypes.Length; x++)
        {
            AfterFight.Add(SetExtraConditions);
        }
        AfterFight[0] = ShopTutorial;
    }
    #endregion

    #region Boss Battle Functions
    public void BossBattle()
    {
        EnemyCharacter EnemyType = characCon.BossEnemyTypes[CurrentBossFight]; 
        combCon.InitiateCombat(characCon.PartyMembers, EnemyType, EnemyType.returnCurrentValues().level , AfterBossBattle);
    }
    public void AfterBossBattle()
    {
        AfterFight[CurrentBossFight]();
        CurrentBossFight++;
        narativeSyst.CheckForNarative();
    }
    public void StartBossBattle()
    {
        characCon.InitiateBatlle(BattleTypes.boss);
        BossFightButton.interactable = false;
    }
    #endregion

    #region Narrative and Tutorial Functions

    public void StartOfTutorial()
    {
        CurrentBossFight = 0;
        worldCon.viewingMap = false;
        WorldAreaManager.currentLocation = WorldLocation.TutorialLocation;
        localAreaCon.LocalAreaObj.SetActive(true);
        localAreaCon.GoToLocalArea(LocalArea.Entrance);
        localAreaCon.LocalAreaTutorial(new bool[] { false, true, false, false, false});
        InventoryController.instance.playerInven.EquipArenaTutorialItems();
        BossFightButton.interactable = true;
    }
    public void ShopTutorial()
    {
        StoreTutorial = true;
        InventoryController.instance.playerInven.UnequipArenaTutorialItems();
        localAreaCon.LocalAreaTutorial(new bool[] { true, false, false, false, false });
        SetExtraConditions();

    }
    public void PurchaseEvent(Item PurchasedItem)
    {
        if(StoreTutorial)
        {
            if(PurchasedItem.name == StartingItemNames[0])
            {
                PurchasedStartingItems[0] = true;
                invenCon.tutorialHasItem[0] = true;
            }
            if(PurchasedItem.name == StartingItemNames[1])
            {
                PurchasedStartingItems[1] = true;
                invenCon.tutorialHasItem[1] = true;
            }
            if(PurchasedStartingItems[0] && PurchasedStartingItems[1])
            {
                SetExtraConditions();
                invenCon.tutorial = false;
            }
        }
    }
    public void TavernTutorial()
    {
        localAreaCon.LocalAreaTutorial(new bool[] { false, false, true, false, false });
        NextQuestAvailable();
    }
    public void EquipArouraFight()
    {
        invenCon.OpenPlayerInventory();
        invenCon.EquipmentTutorial = true;
        combCon.IfLose = RespawnFromAurora;
    }
    public void MapTutorial()
    {
        AlertMessage.instance.ActivateMessage("Aurora has joined your party");
        characCon.AddNextMemberToParty();
        localAreaCon.LocalAreaTutorial(new bool[] { true, true, true, true, true });
        combCon.IfLose = null;
    }
    public void SetExtraConditions()
    {
        narativeSyst.DifferentDialogues[narativeSyst.narativePosition].EventConditions.ExtraConditions = true;
    }
    public void RevealNextLocation()
    {
        worldCon.RevealNextLocation();
    }
    public void AfterGreaeFight()
    {
        RevealNextLocation();
        RevealNextLocation();
        NextQuestAvailable();
        combCon.IfLose = null;
    }
    public void NextQuestAvailable()
    {
        SetExtraConditions();
        isNextQuest = true;
        QuestButton.interactable = true;
    }
    public void StartNextQuest()
    {
        if (isNextQuest)
        {
            QuestButton.interactable = false;
            SetExtraConditions();
            narativeSyst.CheckForNarative();
            isNextQuest = false;
        }

    }
    public void ArenaBossBattle()
    {
        BossFightButton.interactable = true;
    }
    public void AfterNemeanLion()
    {
        AlertMessage.instance.ActivateMessage("Delvon has joined your party");
        characCon.AddNextMemberToParty();
        NextQuestAvailable();
        combCon.IfLose = null;
    }
    public void AfterDragon()
    {
        Utilities.ScreenShake(2, 20, Vector3.up, true, true);
        StartCoroutine(FadeToBlack(AfterDragonFade));
        combCon.IfLose = RespawnFromHydraBattles;
    }
    public void MobFight()
    {
        StartBossBattle();
    }
    public void AfterMobFight()
    {
        worldCon.ToggleWorldMap();
        localAreaCon.LocalAreaTutorial(new bool[] { true, true, true, false, true });
        worldCon.GotoWorldLocation(WorldLocation.Braaumfontein, false, false);
        localAreaCon.GoToLocalArea(LocalArea.Entrance);
        narativeSyst.CheckForNarative();
        combCon.IfLose = null;
    }
    private void AfterDragonFade()
    {
        worldCon.ToggleWorldMap();
        worldCon.GotoWorldLocation(WorldLocation.Colchis, false);
        localAreaCon.GoToLocalArea(LocalArea.Entrance);
    }
    public void EnterOracleTempleFade()
    {
        narativeSyst.DifferentDialogues[narativeSyst.narativePosition + 1].EventConditions.ExtraConditions = true;
        StartCoroutine(FadeToBlack(narativeSyst.CheckForNarative));
       
    }
    public void HealEffect()
    {
        StartCoroutine(FadeToBlack(PlayHealSound));
    }
    public void PlayHealSound()
    {
        
    }
    public void BeforeGraeae()
    {
        combCon.IfLose = RespawnFromGraeae;
        StartBossBattle();
    }
    private IEnumerator FadeToBlack(bossDelegate inbetweenFunc)
    {
        FadeScreen.SetActive(true);
        yield return new WaitForSeconds(1);
        if(inbetweenFunc != null            )
            inbetweenFunc();
        yield return new WaitForSeconds(1);
        FadeScreen.SetActive(false);
    } 
    public void AfterTavernArgument()
    {
        RevealNextLocation();
        localAreaCon.LocalAreaTutorial(new bool[] { true, true, true, true, true });
    }
    public void TheBigThreeAppear()
    {
        // play rumble sound
        combCon.IfLose = RespawnFromGodBattles;
    }
    public void FightAthenaFinal()
    {
        characCon.RemovePartyMembers();
        invenCon.playerInven.AddPlayerInven(Vanquisher);
        invenCon.OpenPlayerInventory();
        invenCon.finalBattle = true;
    }
    public void RespawnFromAurora()
    {
        RespawnFromBossFight(4);
        invenCon.playerInven.RemoveEquipment(EquipSlots.Weapon);
        invenCon.playerInven.RemoveEquipment(EquipSlots.Armour_Body);
    }
    public void EndofNarrative()
    {
        characCon.AddNextMemberToParty();
        characCon.AddNextMemberToParty();
        StartCoroutine(FadeToBlack(null));
        worldCon.ToggleWorldMap();
        worldCon.GotoWorldLocation(WorldLocation.TutorialLocation,false);
        combCon.IfLose = null;
    }
    public void BeforeNeameanLion()
    {
        ArenaBossBattle();
        combCon.IfLose = RespawnFromNemeanLion;
    }
    public void RespawnFromNemeanLion()
    {
        RespawnFromBossFight(14);
    }
    public void RespawnFromHydraBattles()
    {
        RespawnFromBossFight(22);

        BossFightButton.interactable = true;
    }
    public void RespawnFromGodBattles()
    {
        RespawnFromBossFight(26);
    }
    public void RespawnFromGraeae()
    {
        RespawnFromBossFight(10);
    }
    private void RespawnFromBossFight(int narrativePos)
    {
        narativeSyst.narativePosition = narrativePos;
        ResetExtraConditions(narrativePos);
        CurrentBossFight--;
    }
    private void ResetExtraConditions(int startPos)
    {
        for (int x = startPos; x < narativeSyst.DifferentDialogues.Length; x++)
        {
            narativeSyst.DifferentDialogues[x].EventConditions.ExtraConditions = false;
        }
    }

    #endregion

}
