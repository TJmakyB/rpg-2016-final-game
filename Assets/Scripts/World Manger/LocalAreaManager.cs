﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum LocalArea { Entrance, Store, Arena, Tavern, WorldMap, None};
public class LocalAreaManager : MonoBehaviour
{
    public static LocalArea currentArea = LocalArea.Entrance;
    public GameObject[] AreaPannels;
    public NarativeSystem NS;
    
    public void GotoLocalAreaButton(Object AreaPanel)
    {
        GameObject area = AreaPanel as GameObject;
        GoToAreaPannel selectedPannel = area.GetComponent<GoToAreaPannel>();
        GoToLocalArea(selectedPannel.areaToGoTo);
    }
    public void GoToLocalArea(LocalArea newArea)
    {
        if (currentArea != newArea)
        {
            int area = (int)newArea;
            int curArea = (int)currentArea;
            AreaPannels[curArea].SetActive(false);
            AreaPannels[area].SetActive(true);
            if (AreaPannels[area] == AreaPannels[3])
            {
                PlayerCharacter[] players = this.GetComponent<CharacterManager>().PlayableCharacters;
                for (int i = 0; i< players.Length; i++)
                {
                    players[i].HealCharacter();
                }
                Debug.Log(players[0].Name + " " + players[0].retCurrentHealth() + " Max of : " + players[0].retMaxHealth());
            }
            currentArea = newArea;
            NS.CheckForNarative();
        }

    }

}