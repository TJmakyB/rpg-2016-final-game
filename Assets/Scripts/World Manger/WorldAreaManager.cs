﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum WorldLocation { TutorialLocation, City1, City2, City3, City4,  Braaumfontein, Colchis, TempleOfTheGraeae, SleepingDragonsDen ,None , TempleOfTheOracle}
public enum WorldMapEnemies { Wolves, Bandits, Harpies };
public class WorldAreaManager : MonoBehaviour
{

    public static WorldLocation currentLocation = WorldLocation.None;
    public GameObject worldMapObj;
    public LocalAreaManager locMan;
    public bool viewingMap = true;
    private NarativeSystem NS;
    public CharacterManager CharacMan;
    private WorldLocation desiredLocation = WorldLocation.None;
    public Button[] LocationButtons;
    public Button[] HiddenLocations;
    public GameObject BattleLocationObj;
    public GameObject localAreaObj;
    private bool isFromBattleLocation = false;
    public WorldLocation prevLocation = WorldLocation.TutorialLocation;

    private int nextHiddenLocation = 0;


    void Start()
    {
        ToggleWorldMap();
        NS = gameObject.GetComponent<NarativeSystem>();
    }
    public void GotoWorldLocationButton(Object worldLoc)
    {
        GameObject newPosObj = worldLoc as GameObject;
        WorldLocation location = newPosObj.GetComponent<WorldAreaPanel>().newLocation;
        bool isBattle = newPosObj.GetComponent<WorldAreaPanel>().isBattleLocation;
        GotoWorldLocation(location, isBattle);

    }
    public void GotoWorldLocation(WorldLocation loc, bool isBattleLocation, bool travelBattle = true)
    {
        desiredLocation = loc;
        if (desiredLocation != currentLocation)
        {
            prevLocation = currentLocation;
            if(desiredLocation == WorldLocation.TempleOfTheOracle)
            {
                if(!InventoryController.instance.playerInven.DoesPlayerHaveQuestItem(QuestItems.wingsOfTheHarpie))
                {
                    AlertMessage.instance.ActivateMessage("Deneva we need to get the wings of some harpies before we can get to the oracle, we should run into some if we keep traveling");
                    return;
                }
            }
            if(isBattleLocation || isFromBattleLocation)
            {               
                MoveToBattleLocation(isFromBattleLocation);
                isFromBattleLocation = isBattleLocation;
                return;
            }
            localAreaObj.SetActive(true);
            BattleLocationObj.SetActive(false);
            isFromBattleLocation = false;
            if (travelBattle)
                MoveBetweenBeforeBattle();
            else
            {
                ToggleWorldMap();
                MoveBetweenAfterBattle();
            }
        }
        else
            AlertMessage.instance.ActivateMessage("You are already there");
    }

    public void MoveToBattleLocation(bool isLocalArea)
    {
        ToggleWorldMap();
        localAreaObj.SetActive(isLocalArea);
        BattleLocationObj.SetActive(!isLocalArea);
        if (isLocalArea)
            locMan.GoToLocalArea(LocalArea.Entrance);
        else
            locMan.GoToLocalArea(LocalArea.None);
        currentLocation = desiredLocation;
        NS.CheckForNarative();
    }
    public void MoveBetweenAfterBattle()
    {

        locMan.GoToLocalArea(LocalArea.Entrance);
        currentLocation = desiredLocation;
        NS.CheckForNarative();
    }
    public void RespawnAtClosestTavern()
    {
        if(isFromBattleLocation)
        {
            ToggleWorldMap();
            GotoWorldLocation(prevLocation, false, false);
            locMan.GoToLocalArea(LocalArea.Tavern);
        }
        else
        {
            locMan.GoToLocalArea(LocalArea.Tavern);
        }
    }
    void MoveBetweenBeforeBattle()
    {
        ToggleWorldMap();
        CharacMan.InitiateBatlle(BattleTypes.travel);
    }

    public void ToggleWorldMap()
    {
        viewingMap = !viewingMap;
        worldMapObj.SetActive(viewingMap);
    }
    public void DisableWorldLoactions(bool[] activebuttons)
    {
        if (activebuttons.Length != LocationButtons.Length)
        {
            Debug.Log("Button lenght Error");
            return;
        }

        for(int x = 0; x < activebuttons.Length; x++)
        {
            LocationButtons[x].interactable = activebuttons[x];
        }

    }

    public void RevealNextLocation()
    {
        if(nextHiddenLocation < HiddenLocations.Length)
        {
            HiddenLocations[nextHiddenLocation].gameObject.SetActive(true);
            nextHiddenLocation++;
        }
    }
}