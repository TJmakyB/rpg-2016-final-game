﻿using UnityEngine;
using System.Collections;

public enum EquipSlots { Weapon , Armour_Helmet, Armour_Body , Accessory};
public enum WeaponType { gem, staff, sword, none }
public enum ArmourType { light, medium, heavy}
public enum Rarity {common,rare,epic, legendary }
// item class used for inheritance
[System.Serializable]
public abstract class Item: ScriptableObject
{
    public new string name;
    public int cost;
    public Rarity rarity;
    public EquipSlots type;
    public Sprite icon;

    [SerializeField]
    private int mobilityReduct; //mobility value adjusts accuracy
    [SerializeField]
    protected int durability;
    public virtual int retDamage() { return 0; }
    public virtual int retAccuracy() { return 0; }
    public virtual int retArmour() { return 0; }
    public int retDurability() { return durability; }
    public int retMobility() { return mobilityReduct; }

}






