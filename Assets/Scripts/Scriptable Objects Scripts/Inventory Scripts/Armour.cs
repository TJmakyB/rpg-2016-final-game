﻿/*
script description:
*/
using UnityEngine;
using System.Collections;

// Armour class inherits from the item class
[System.Serializable]
[CreateAssetMenu(menuName = "Items/Armour")]
public class Armour : Item
{
    [SerializeField]
    private int armour; //armour value adjusts health 

    public ArmourType armourType;
    /// <summary>
    /// returns the amount of armour the item has
    /// </summary>
    /// <returns></returns>
    public override int retArmour() { return armour; }
    /// <summary>
    /// returns the amount of mobility
    /// </summary>
    /// <returns></returns>

}