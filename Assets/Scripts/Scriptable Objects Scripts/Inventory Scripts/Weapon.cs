﻿/*
script description:
*/
using UnityEngine;
using System.Collections;

// Weapon class for weapons 
[System.Serializable]
[CreateAssetMenu(menuName = "Items/Weapon")]
public class Weapon : Item
{
    [SerializeField]
    private int damage;
    [SerializeField]
    private int accuracy;

    public WeaponType weaponType;

    public override int retDamage() { return damage; }
    public override int retAccuracy() { return accuracy; }

}