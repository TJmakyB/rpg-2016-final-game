﻿/*
script description:
*/
using UnityEngine;
using System.Collections;

public abstract class QuestEvent : ScriptableObject
{
    public Reward QuestReward;

    public virtual void RewardPlayer()
    {
        if(AreConditionsMet())
        {
            
        }
    }
    public abstract bool AreConditionsMet();
}

[System.Serializable]
public abstract class Reward
{

}

public class BattleReward:Reward
{
    public int goldAmount;
    public Item itemReward;
}




