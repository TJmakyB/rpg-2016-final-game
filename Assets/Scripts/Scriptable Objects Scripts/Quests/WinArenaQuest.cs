﻿/*
script description:
*/
using UnityEngine;
using System.Collections;

public class WinArenaQuest : QuestEvent
{
    private bool isArenaWon;
    public Conditions ArenaLocation; 

    public override bool AreConditionsMet()
    {
        if (isArenaWon)
            return true;
        return false;
    }
}