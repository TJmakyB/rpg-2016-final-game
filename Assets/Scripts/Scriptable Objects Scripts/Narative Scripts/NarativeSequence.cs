﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Narative/Narative Location Sequence")]
public class NarativeSequence:ScriptableObject
{
    public List<DialogueElement> dialogueEvent = new List<DialogueElement>();
    public Conditions EventConditions;

    public virtual bool ConditionsMet()
    {
        if (EventConditions.NeedsExtraConditions)
        {
            if (isLocationCorrect() && EventConditions.ExtraConditions)
                return true;
            else
                return false;
        }
        else
            return isLocationCorrect();
    }

    private bool isLocationCorrect()
    {
        Conditions currentCond = EventConditions;
        if ((currentCond.inWorldLoc == WorldAreaManager.currentLocation || currentCond.inWorldLoc == WorldLocation.None)&&
            currentCond.inLocalLoc == LocalAreaManager.currentArea)
            return true;
        else
            return false;
    }
    public void Initialise()
    {
        EventConditions.ExtraConditions = false;
    }

}

