﻿/*
script description:
*/
using UnityEngine;
using System.Collections;
[CreateAssetMenu(menuName = "Narative/Dialogue Element")]
public class DialogueElement:ScriptableObject
{
    public string Name;
    [TextArea(3, 10)]
    public string Dialogue;
    public bool isBlockText;
}