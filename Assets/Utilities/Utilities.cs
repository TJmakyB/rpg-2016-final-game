﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utilities
{

    public delegate bool CompareTo<T>(T obj1, T obj2);
    public static int BruteForceSearch<T>(List<T> list, T obj, CompareTo<T> comparison)
    {
        for(int x = 0; x < list.Count; x++ )
        {
            if(comparison(list[x], obj))
            {
                return x;
            }
        }
        return -1;
    }

    public static int BruteForceSearch<T>(T[] array, T obj, CompareTo<T> comparison)
    {
        for (int x = 0; x < array.Length; x++)
        {
            if (comparison(array[x], obj))
            {
                return x;
            }
        }
        return -1;
    }

    static public void ScreenShake(float time, float intensity, Vector3 Direc, bool _3Dshake = true, bool randDir = false)
    {
        if (randDir)
        {
            if (_3Dshake)
                Direc = Random.insideUnitSphere.normalized * intensity;
            else
                Direc = Random.insideUnitCircle.normalized * intensity;
        }
        else
            Direc = Direc.normalized * intensity;
            iTween.ShakePosition(Camera.main.gameObject,iTween.Hash("amount", Direc ,"islocal", true, "time", time));    
    }

    static public void ScreenWobble(float time, float intensity, Vector3 Direc, bool _3Dwobble = true, bool randDir = false)
    {
        if (randDir)
        {
            if (_3Dwobble)
                Direc = Random.insideUnitSphere.normalized * intensity;
            else
                Direc = Random.insideUnitCircle.normalized * intensity;
        }
        else
            Direc = Direc.normalized * intensity;
        iTween.ShakeRotation(Camera.main.gameObject, iTween.Hash("amount", Direc, "islocal", true, "time", time));    
    }

    static public void PlaySound(AudioSource audioSource, AudioClip clip, bool checkPlaying = true, float timePosition = 0)
    {
        if (checkPlaying)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.time = timePosition;
                audioSource.clip = clip;
                audioSource.Play();
            }
        }
        else
        {
            audioSource.Stop();
            audioSource.PlayOneShot(clip);
        }

    }


}
